#!/bin/sh
VERSION=`grep versionName app/build.gradle | cut -d'"' -f2 | sed "s/\\./_/g"`
PROJNAME=edriver

./gradlew assembleDebug
mv app/build/outputs/apk/debug/app-debug.apk $PROJNAME-$VERSION-$BUILD_NUMBER.apk
