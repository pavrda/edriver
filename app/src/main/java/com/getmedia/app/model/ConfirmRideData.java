package com.hitasoft.app.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hitasoft on 22/4/18.
 */

public class ConfirmRideData {
    @SerializedName("status")
    public String status;

    @SerializedName("message")
    public String message;

    @SerializedName("pickup_location")
    public String pickup_location;

    @SerializedName("pickup_lat")
    public String pickup_lat;

    @SerializedName("pickup_lng")
    public String pickup_lng;

    @SerializedName("drop_location")
    public String drop_location;

    @SerializedName("drop_lat")
    public String drop_lat;

    @SerializedName("drop_lng")
    public String drop_lng;

    @SerializedName("onride_otp")
    public String onride_otp;

    @SerializedName("driver_name")
    public String driver_name;

    @SerializedName("driver_id")
    public String driver_id;

    @SerializedName("driver_rating")
    public String driver_rating;

    @SerializedName("driver_mobile")
    public String driver_mobile;

    @SerializedName("driver_vehicle")
    public String driver_vehicle;

    @SerializedName("driver_vehicleno")
    public String driver_vehicleno;

    @SerializedName("base_price")
    public String base_price;

    @SerializedName("driver_image")
    public String driver_image;
}
