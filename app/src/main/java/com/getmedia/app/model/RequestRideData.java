package com.hitasoft.app.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hitasoft on 22/4/18.
 */

public class RequestRideData {
    @SerializedName("status")
    public String status;

    @SerializedName("message")
    public String message;

    @SerializedName("onride_id")
    public String onride_id;

    @SerializedName("onride_type")
    public String onride_type;
}
