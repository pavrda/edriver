package com.hitasoft.app.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by hitasoft on 25/2/18.
 */

public class AvailableRide {
    @SerializedName("status")
    public String status;

    @SerializedName("message")
    public String message;

    @SerializedName("available_ride")
    public ArrayList<result> result = new ArrayList<>();

    public class result {

        @SerializedName("_id")
        public String id;

        @SerializedName("category_name")
        public String category_name;

        @SerializedName("image")
        public String category_image;

        @SerializedName("baseprice")
        public String baseprice;

        @SerializedName("reach_pickup")
        public String reach_pickup;
    }
}

