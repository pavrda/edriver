package com.hitasoft.app.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hitasoft on 22/3/18.
 */

public class UserData {

    @SerializedName("status")
    public String status;

    @SerializedName("message")
    public String message;

    @SerializedName("user_id")
    public String user_id;

    @SerializedName("full_name")
    public String full_name;

    @SerializedName("user_image")
    public String imageurl = null;

    @SerializedName("email")
    public String email;

    @SerializedName("password")
    public String password;

    @SerializedName("country_code")
    public String country_code;

    @SerializedName("phone_number")
    public String phone_number;

    @SerializedName("token")
    public String token;

    @SerializedName("walletmoney")
    public String walletmoney;

}
