package com.hitasoft.app.cabso;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hitasoft.app.cabso.databinding.ActivityEmergencyContactBinding;
import com.hitasoft.app.cabso.databinding.EmergencyContactItemBinding;
import com.hitasoft.app.model.ProfileData;
import com.hitasoft.app.utils.ApiClient;
import com.hitasoft.app.utils.ApiInterface;
import com.hitasoft.app.utils.Constants;
import com.hitasoft.app.utils.GetSet;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hitasoft on 20/3/18.
 */

public class EmergencyContactActivity extends AppCompatActivity implements View.OnClickListener {
    static final String TAG = "EmergencyContactAct";
    ImageView backBtn;
    ActivityEmergencyContactBinding binding;
    Display display;
    ProgressDialog progressDialog;
    RelativeLayout nullLay;
    TextView nullText;
    ImageView nullImage;

    RecyclerAdapter recyclerAdapter;
    LinearLayoutManager linearLayoutManager;
    ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_contact);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_emergency_contact);
        backBtn = findViewById(R.id.backBtn);
        nullLay = findViewById(R.id.nullLay);
        nullText = findViewById(R.id.nullText);
        nullImage = findViewById(R.id.nullImage);

        display = this.getWindowManager().getDefaultDisplay();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.pleasewait));
        backBtn.setVisibility(View.VISIBLE);

        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        binding.recyclerView.setLayoutManager(linearLayoutManager);


        backBtn.setOnClickListener(this);
        binding.addContact.setOnClickListener(this);

        recyclerAdapter = new RecyclerAdapter(EmergencyContactActivity.this, ProfileActivity.emergencyList);
        binding.recyclerView.setAdapter(recyclerAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.v("EmergencyContactAc", "Emergency List-list=" + ProfileActivity.emergencyList);
        if (recyclerAdapter != null)
            recyclerAdapter.notifyDataSetChanged();
        if (ProfileActivity.emergencyList.size() <= 0) {
            nullLay.setVisibility(View.VISIBLE);
            nullText.setText(getString(R.string.no_contacts));
        } else {
            nullLay.setVisibility(View.GONE);
        }
    }

    private JSONArray listToJSONArray(ArrayList<ProfileData.EmergencyContact> list) {
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < list.size(); i++) {
            ProfileData.EmergencyContact emergencyContact = list.get(i);
            HashMap hashMap = new HashMap();
            hashMap.put("name", emergencyContact.name);
            hashMap.put("phone_no", emergencyContact.phone_no);
            JSONObject jsonObject = new JSONObject(hashMap);
            jsonArray.put(jsonObject);
        }
        return jsonArray;
    }

    /*API Integration*/

    /**
     * Function to Edit/Delete Emergency Contact
     */

    private void emergencyContact(JSONArray jsonObject) {
        Map<String, String> map = new HashMap<>();
        map.put(Constants.TAG_USER_ID, GetSet.getUserId());
        map.put(Constants.TAG_EMERGENCY_CONTACT, jsonObject.toString());
        progressDialog.show();
        Log.v(TAG, "emergencyContactParams:" + map);
        Call<Map<String, String>> call3 = apiInterface.updateProfile(GetSet.getToken(), map);
        call3.enqueue(new Callback<Map<String, String>>() {
            @Override
            public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                Map<String, String> data = response.body();
                if (data.get(Constants.TAG_STATUS).equals("true")) {
                    Log.v(TAG, "emergencyContactRes=" + data.get(Constants.TAG_MESSSAGE));
                    progressDialog.dismiss();
                    Toast.makeText(EmergencyContactActivity.this, data.get(Constants.TAG_MESSSAGE), Toast.LENGTH_SHORT).show();
                    recyclerAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<Map<String, String>> call, Throwable t) {
                call.cancel();
            }
        });
    }

    /**
     * Function for OnClick Event
     */

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backBtn:
                onBackPressed();
                break;
            case R.id.addContact:
                Intent i = new Intent(getApplicationContext(), AddContactActivity.class);
                i.putExtra("from", "add");
                startActivity(i);
        }
    }

    /**
     * Adapter for Emergency Contacts
     */

    private class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.EmContactViewHolder> {
        ArrayList<ProfileData.EmergencyContact> phoneNumberList;
        Context context;

        public RecyclerAdapter(Context context, ArrayList<ProfileData.EmergencyContact> items) {
            phoneNumberList = items;
            this.context = context;
        }

        @Override
        public EmContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            EmergencyContactItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.emergency_contact_item, parent, false);
            return new EmContactViewHolder(binding);
        }

        @Override
        public void onBindViewHolder(EmContactViewHolder viewHolder, final int position) {
            viewHolder.getBinding().name.setText(phoneNumberList.get(position).name);
            viewHolder.getBinding().phoneNumber.setText(phoneNumberList.get(position).phone_no);
            viewHolder.getBinding().more.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("NewApi")
                @Override
                public void onClick(View v) {
                    TextView edit, delete;
                    LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
                    final View popupView = layoutInflater.inflate(R.layout.address_popup_window, null);
                    final PopupWindow popup = new PopupWindow(context);
                    popup.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    popup.setContentView(popupView);
                    popup.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
                    popup.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
                    popup.setFocusable(true);
                    popup.setElevation(10);
                    popup.showAsDropDown(v, -((display.getWidth() * 18 / 100)), -30);
                    edit = (TextView) popupView.findViewById(R.id.edit);
                    delete = (TextView) popupView.findViewById(R.id.delete);
                    edit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            popup.dismiss();
                            Intent i = new Intent(getApplicationContext(), AddContactActivity.class);
                            i.putExtra("from", "update");
                            i.putExtra("updatedPosition", position);
                            i.putExtra("oldName", phoneNumberList.get(position).name);
                            i.putExtra("oldPhno", phoneNumberList.get(position).phone_no);
                            startActivity(i);
                        }
                    });
                    delete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ProfileActivity.emergencyList.remove(position);
                            popup.dismiss();
                            JSONArray jsonObject = listToJSONArray(ProfileActivity.emergencyList);
                            emergencyContact(jsonObject);
                            if (ProfileActivity.emergencyList.size() <= 0) {
                                nullLay.setVisibility(View.VISIBLE);
                                nullText.setText(getString(R.string.no_contacts));
                            }
                        }
                    });
                }
            });
        }

        @Override
        public int getItemCount() {
            return phoneNumberList.size();
        }

        public class EmContactViewHolder extends RecyclerView.ViewHolder {
            EmergencyContactItemBinding emContactItemBinding;

            public EmContactViewHolder(EmergencyContactItemBinding layoutBinding) {
                super(layoutBinding.getRoot());
                emContactItemBinding = layoutBinding;
            }

            public EmergencyContactItemBinding getBinding() {
                return emContactItemBinding;
            }
        }
    }
}
