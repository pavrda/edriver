package com.hitasoft.app.cabso;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;

import com.hitasoft.app.cabso.databinding.ActivityWelcomeBinding;

public class WelcomeActivity extends AppCompatActivity implements View.OnClickListener {
    ActivityWelcomeBinding binding;
    Snackbar snackbar;
    int exit = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_welcome);
        binding.login.setOnClickListener(this);
        binding.signup.setOnClickListener(this);

        if (SplashActivity.displayHeight != 0) {
            ViewGroup.LayoutParams params = binding.parent.getLayoutParams();
            // Changes the height and width to the specified *pixels*
            params.height = SplashActivity.displayHeight;
            binding.parent.setLayoutParams(params);
        }
        snackbar = Snackbar.make(findViewById(R.id.parent), getString(R.string.exit_msg), Snackbar.LENGTH_SHORT);
        snackbar.addCallback(new Snackbar.Callback() {
            @Override
            public void onDismissed(Snackbar snackbar, int event) {
                exit = 0;
            }

            @Override
            public void onShown(Snackbar snackbar) {
                exit = 1;
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login:
                Intent b = new Intent(WelcomeActivity.this, LoginActivity.class);
                startActivity(b);
                break;
            case R.id.signup:
                Intent intent = new Intent(WelcomeActivity.this, SignupActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (exit == 0) {
            snackbar.show();
        } else {
            WelcomeActivity.this.finishAffinity();
        }
    }
}
