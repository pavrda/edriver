package com.hitasoft.app.cabso;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.SquareCap;
import com.google.gson.Gson;
import com.hitasoft.app.cabso.databinding.CategoryItemBinding;
import com.hitasoft.app.helper.DirectionsJSONParser;
import com.hitasoft.app.helper.NetworkReceiver;
import com.hitasoft.app.model.AvailableRide;
import com.hitasoft.app.model.ConfirmRideData;
import com.hitasoft.app.model.RequestRideData;
import com.hitasoft.app.utils.ApiClient;
import com.hitasoft.app.utils.ApiInterface;
import com.hitasoft.app.utils.Constants;
import com.hitasoft.app.utils.GetSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static com.bumptech.glide.request.RequestOptions.circleCropTransform;
import static com.google.android.gms.maps.model.JointType.ROUND;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback,
        DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, View.OnClickListener, LocationListener,
        NetworkReceiver.ConnectivityReceiverListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    RelativeLayout bottomLay, paymentLay, cardlay, cashlay, nodriverLay, noServiceLay, driverDetailLay;
    ArrayList markerPoints = new ArrayList();
    String pickuppoint, from, droppoint, type, payment_type, onride_id, distanceValue = "", durationValue = "", basePrice = "";
    Double pickuplat, pickuplng, droplat, droplng;
    long timestamp;
    ArrayList<HashMap<String, String>> cabnearby = new ArrayList<>();
    ArrayList<String> driverid = new ArrayList<>();
    ArrayList<Marker> markers = new ArrayList<>();
    NetworkReceiver receiver;
    int searchcount = 0;
    Marker arrivingdriver;
    ApiInterface apiInterface;
    ImageView backBtn, cardselect, cashselect, driverImage;
    RatingBar ratingBar;
    RecyclerView categoryList;
    CategoryAdapter categoryAdapter;
    Animation animZoomin;
    TextView approxprice, approxprice2, ridenow, confirmride, sosAlert, approxAmount, driverName, vehicleDetail,
            ratingCount, driverId, otp, cancelRide, callDriver, distance;
    View statusView;
    LinearLayout ridelater;
    ArrayList<AvailableRide.result> availabeAry = new ArrayList<>();
    AvailableRide rideData;
    Socket mSocket;
    int year, month, day;
    int selectedpos = 0;
    Dialog dialog;
    Toolbar toolbar;
    DatePickerDialog datePickerDialog;
    TimePickerDialog timePickerDialog;
    Animation slideup, slidedown;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Handler handler = new Handler();
    private double lat, lng;
    private List<LatLng> listLatLng = new ArrayList<>();
    private GoogleMap mMap;
    private Polyline blackPolyLine, greyPolyLine;
    Animator.AnimatorListener polyLineAnimationListener = new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animator) {

        }

        @Override
        public void onAnimationEnd(Animator animator) {

            List<LatLng> blackLatLng = blackPolyLine.getPoints();
            List<LatLng> greyLatLng = greyPolyLine.getPoints();

            greyLatLng.clear();
            greyLatLng.addAll(blackLatLng);
            blackLatLng.clear();

            blackPolyLine.setPoints(blackLatLng);
            greyPolyLine.setPoints(greyLatLng);

            blackPolyLine.setZIndex(2);

            animator.start();
        }

        @Override
        public void onAnimationCancel(Animator animator) {

        }

        @Override
        public void onAnimationRepeat(Animator animator) {

        }
    };

    private Emitter.Listener hereListener = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                public void run() {
                    try {
                        Log.v("emit", "hereListener=" + args[0]);
                        JSONObject data = (JSONObject) args[0];
                        String instant_lat = data.getString("instant_lan");
                        String instant_lng = data.getString("instant_lng");
                        String ridestatus = data.getString("ridestatus");

                        Handler handler = new Handler();
                        Runnable myRunnable = new Runnable() {
                            public void run() {
                                try {
                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("onride_id", onride_id);
                                    mSocket.emit("whereareyou", jsonObject);
                                    Log.v("Whereareu", "" + jsonObject);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        };
                        if (ridestatus.equals("onride")) {
                            if (arrivingdriver != null)
                                arrivingdriver.remove();
                            if (driverDetailLay.getVisibility() != View.INVISIBLE) {
                                driverDetailLay.setVisibility(View.INVISIBLE);
                                driverDetailLay.startAnimation(slidedown);
                            }
                            handler.postDelayed(myRunnable, 5000);
                        } else if (ridestatus.equals("completed")) {
                            handler.removeCallbacks(myRunnable);
                            finish();
                            Intent i = new Intent(MapsActivity.this, RideComplete.class);
                            i.putExtra("onride_id", onride_id);
                            startActivity(i);
                        } else if (ridestatus.equals("accepted") || ridestatus.equals("ontheway")) {
                            removeAlldrivers();
                            updateArriving(instant_lat, instant_lng);
                            handler.postDelayed(myRunnable, 5000);

                        } else {
                            handler.postDelayed(myRunnable, 5000);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private void updateArriving(String instant_lat, String instant_lng) {
        if (arrivingdriver == null) {
            arrivingdriver = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(Double.valueOf(instant_lat), Double.valueOf(instant_lng)))
                    .anchor(0.5f, 0.5f)
                    .title("Your Driver")
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));
        } else {
            LatLng startPosition = arrivingdriver.getPosition();
            LatLng endPosition = new LatLng(Double.valueOf(instant_lat), Double.valueOf(instant_lat));

            if (startPosition.latitude != endPosition.latitude) {
                Log.v("BA", "---------");
                ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
                valueAnimator.setDuration(2000);
                valueAnimator.setInterpolator(new LinearInterpolator());
                valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        float v = valueAnimator.getAnimatedFraction();
                        lng = v * endPosition.longitude + (1 - v)
                                * startPosition.longitude;
                        lat = v * endPosition.latitude + (1 - v)
                                * startPosition.latitude;
                        LatLng newPos = new LatLng(lat, lng);

                        if (getBearing(startPosition, newPos) != -1) {
                            arrivingdriver.setPosition(newPos);
                            arrivingdriver.setAnchor(0.5f, 0.5f);
                            arrivingdriver.setRotation(getBearing(startPosition, newPos));
                        }
                    }
                });
                valueAnimator.start();
            }
        }
    }

    private Emitter.Listener cabNearBy = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                public void run() {
                    try {
                        Log.v("ON", "cabNearBy=" + args[0]);
                        JSONArray data = (JSONArray) args[0];
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject obj = data.getJSONObject(i);
                            String driver_id = obj.getString("id");
                            String lat = obj.getString("lat");
                            String lng = obj.getString("lng");
                            HashMap<String, String> map = new HashMap<>();
                            map.put("driver_id", driver_id);
                            map.put("lat", lat);
                            map.put("lng", lng);
                            updatemarkers(map);
                            cabnearby.add(map);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        receiver = new NetworkReceiver();
        ApplicationClass.getInstance().setConnectivityListener(this);
        bottomLay = findViewById(R.id.bottomLay);
        cardlay = findViewById(R.id.cardlay);
        cashlay = findViewById(R.id.cashlay);
        cardselect = findViewById(R.id.cardselect);
        cashselect = findViewById(R.id.cashselect);
        paymentLay = findViewById(R.id.paymentLay);
        categoryList = findViewById(R.id.categoryList);
        backBtn = findViewById(R.id.backBtn);
        approxprice = findViewById(R.id.approxprice);
        approxprice2 = findViewById(R.id.approxprice2);
        ridenow = findViewById(R.id.ridenow);
        confirmride = findViewById(R.id.confirmride);
        ridelater = findViewById(R.id.ridelater);
        noServiceLay = findViewById(R.id.noServiceLay);
        nodriverLay = findViewById(R.id.nodriverLay);
        toolbar = findViewById(R.id.toolbar);
        statusView = findViewById(R.id.statusView);
        sosAlert = findViewById(R.id.sosAlert);
        driverDetailLay = findViewById(R.id.driverDetailLay);
        driverImage = findViewById(R.id.driverImage);
        approxAmount = findViewById(R.id.approxAmount);
        driverName = findViewById(R.id.driverName);
        vehicleDetail = findViewById(R.id.vehicleDetail);
        ratingCount = findViewById(R.id.ratingCount);
        driverId = findViewById(R.id.driverId);
        otp = findViewById(R.id.otp);
        cancelRide = findViewById(R.id.cancelRide);
        callDriver = findViewById(R.id.callDriver);
        distance = findViewById(R.id.distance);
        ratingBar = findViewById(R.id.ratingBar);

        slideup = AnimationUtils.loadAnimation(this, R.anim.slide_up);
        slidedown = AnimationUtils.loadAnimation(this, R.anim.slide_down);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        animZoomin = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in);
        dialog = new Dialog(MapsActivity.this, R.style.CusDialog);

        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable().getCurrent();
        stars.getDrawable(2).setColorFilter(getResources().getColor(R.color.notification), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(0).setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(2).setColorFilter(getResources().getColor(R.color.notification), PorterDuff.Mode.SRC_ATOP);

        Calendar cal = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, R.style.datepickerCustom,
                MapsActivity.this, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));

        timePickerDialog = new TimePickerDialog(this, R.style.datepickerCustom,
                MapsActivity.this, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), false);

        pickuppoint = getIntent().getExtras().getString("pickuppoint");
        pickuplat = getIntent().getExtras().getDouble("pickuplat");
        pickuplng = getIntent().getExtras().getDouble("pickuplng");
        droppoint = getIntent().getExtras().getString("droppoint");
        droplat = getIntent().getExtras().getDouble("droplat");
        droplng = getIntent().getExtras().getDouble("droplng");
        from = getIntent().getExtras().getString("from");


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        ApplicationClass app = (ApplicationClass) MapsActivity.this.getApplication();
        mSocket = app.getSocket();
        mSocket.on("cabnearby", cabNearBy);
        mSocket.on("iamhere", hereListener);
        mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.v("EVENT_CONNECT", "EVENT_CONNECT");
            }
        }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.v("EVENT_DISCONNECT", "EVENT_DISCONNECT");
            }
        });
        mSocket.connect();

        LinearLayoutManager CategorylayManager = new LinearLayoutManager(MapsActivity.this, LinearLayoutManager.HORIZONTAL, false);
        categoryList.setLayoutManager(CategorylayManager);
        categoryAdapter = new CategoryAdapter(MapsActivity.this, availabeAry);
        categoryList.setAdapter(categoryAdapter);
        availabeAry.clear();
        new getDistancenDirection().execute();

        backBtn.setOnClickListener(this);
        cardlay.setOnClickListener(this);
        cashlay.setOnClickListener(this);
        ridenow.setOnClickListener(this);
        confirmride.setOnClickListener(this);
        ridelater.setOnClickListener(this);
        cancelRide.setOnClickListener(this);
        sosAlert.setOnClickListener(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setRotateGesturesEnabled(false);
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(this, R.raw.mapstyle));
            if (!success) {
                Log.e("error", "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("error", "Can't find style. Error: ", e);
        }

        Bitmap pickupBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.pickup_marker);
        pickupBitmap = scaleBitmap(pickupBitmap, 50, 100);
        Bitmap dropBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.drop_marker);
        dropBitmap = scaleBitmap(dropBitmap, 50, 100);

        LatLng pickuplatng = new LatLng(pickuplat, pickuplng);
        LatLng droplatng = new LatLng(droplat, droplng);
        markerPoints.add(pickuplatng);
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        MarkerOptions options = new MarkerOptions();
        options.position(pickuplatng);
        options.icon(BitmapDescriptorFactory.fromBitmap(pickupBitmap));
        builder.include(pickuplatng);
        mMap.addMarker(options);
        markerPoints.add(droplatng);
        options.position(droplatng);
        builder.include(droplatng);
        options.icon(BitmapDescriptorFactory.fromBitmap(dropBitmap));
        mMap.addMarker(options);
        searchcount = 0;
        String url = getDirectionsUrl(pickuplatng, droplatng);

        DownloadTask downloadTask = new DownloadTask();
        // Start downloading json data from Google Directions API
        downloadTask.execute(url);

        if (from != null) {
            onride_id = getIntent().getExtras().getString("onrideid");
            basePrice = getIntent().getExtras().getString("baseprice");
            if (from.equals("onride")) {
                //   confirmRide();

                sosAlert.setVisibility(View.VISIBLE);

            } else if (from.equals("ontheway") || from.equals("accepted")) {
                try {
                    sosAlert.setVisibility(View.VISIBLE);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("onride_id", onride_id);
                    mSocket.emit("whereareyou", jsonObject);
                    confirmRide();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        } else {
            // bottomLay.setVisibility(View.VISIBLE);
            //mMap.setPadding(0, 0, 0, bottomLay.getHeight());
        }
        handler.post(cabneargo);

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
        }

        try {
            // begin new code:
            int width = getResources().getDisplayMetrics().widthPixels;
            int height = getResources().getDisplayMetrics().heightPixels;
            int padding = (int) (width * 0.12); // offset from edges of the map 12% of screen

            LatLngBounds bounds = builder.build();
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
            googleMap.moveCamera(cu);
            googleMap.animateCamera(cu);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updatemarkers(HashMap<String, String> data) {
        Log.v("DAta", "" + data.toString());

        if (driverid.isEmpty() || !driverid.contains(data.get("driver_id"))) {
            Marker marker1 = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(Double.valueOf(data.get("lat")), Double.valueOf(data.get("lng"))))
                    .anchor(0.5f, 0.5f)
                    .title(data.get("driver_id"))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));
            markers.add(marker1);
            driverid.add(data.get("driver_id"));
        } else {

            Marker marker1 = markers.get(driverid.indexOf(data.get("driver_id")));
            LatLng startPosition = marker1.getPosition();
            LatLng endPosition = new LatLng(Double.valueOf(data.get("lat")), Double.valueOf(data.get("lng")));
/*
            DownloadTaskLoc downloadTask = new DownloadTaskLoc();
            // Start downloading json data from Google Directions API
            downloadTask.execute(url, data.get("driver_id"));*/
            if (startPosition.latitude != endPosition.latitude) {
                ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
                valueAnimator.setDuration(2000);
                valueAnimator.setInterpolator(new LinearInterpolator());
                valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        float v = valueAnimator.getAnimatedFraction();
                        lng = v * endPosition.longitude + (1 - v)
                                * startPosition.longitude;
                        lat = v * endPosition.latitude + (1 - v)
                                * startPosition.latitude;
                        LatLng newPos = new LatLng(lat, lng);


                        if (getBearing(startPosition, newPos) != -1) {
                            marker1.setPosition(newPos);
                            marker1.setAnchor(0.5f, 0.5f);
                            marker1.setRotation(getBearing(startPosition, newPos));
                        }
                    }
                });
                valueAnimator.start();
            }
        }
    }

    private float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude > end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude > end.latitude && begin.longitude > end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude > end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);

        return -1;
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "mode=driving";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
        return url;
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();
        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private void animatePolyLine() {

        ValueAnimator animator = ValueAnimator.ofInt(0, 100);
        animator.setDuration(1000);
        animator.setInterpolator(new LinearInterpolator());
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animator) {

                List<LatLng> latLngList = blackPolyLine.getPoints();
                int initialPointSize = latLngList.size();
                int animatedValue = (int) animator.getAnimatedValue();
                int newPoints = (animatedValue * listLatLng.size()) / 100;

                if (initialPointSize < newPoints) {
                    latLngList.addAll(listLatLng.subList(initialPointSize, newPoints));
                    blackPolyLine.setPoints(latLngList);
                }
            }
        });
        animator.addListener(polyLineAnimationListener);
        animator.start();

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(MapsActivity.this, ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    //Toast.makeText(MapsActivity.this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                onBackPressed();
                break;
            case R.id.ridenow:
                type = "ridenow";
                bottomLay.setVisibility(View.INVISIBLE);
                bottomLay.startAnimation(slidedown);
                paymentLay.setVisibility(View.VISIBLE);
                paymentLay.startAnimation(slideup);
                mMap.setPadding(0, 0, 0, paymentLay.getHeight());
                approxprice2.setText(getString(R.string.approxprice) + " : " + availabeAry.get(selectedpos).baseprice);
                break;
            case R.id.cardlay:
                payment_type = "card";
                cardselect.setVisibility(View.VISIBLE);
                cashselect.setVisibility(View.GONE);
                break;
            case R.id.cashlay:
                payment_type = "cash";
                cashselect.setVisibility(View.VISIBLE);
                cardselect.setVisibility(View.GONE);
                break;
            case R.id.confirmride:
                if (cashselect.getVisibility() == View.VISIBLE || cardselect.getVisibility() == View.VISIBLE) {
                    paymentLay.setVisibility(View.INVISIBLE);
                    paymentLay.startAnimation(slidedown);
                    if (!type.equals("schedule"))
                        findingDriverDialog("findingDriver");
                    requestRide();
                } else {
                    Toast.makeText(this, getString(R.string.payment_error), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.ridelater:
                type = "schedule";
                Calendar cal = Calendar.getInstance();
                long now = System.currentTimeMillis() - 1000;
                cal.add(Calendar.DAY_OF_YEAR, 6);
                datePickerDialog.setTitle("Schedule Date");
                datePickerDialog.getDatePicker().setMinDate(now);
                datePickerDialog.getDatePicker().setMaxDate(cal.getTimeInMillis()); //After 7 Days from Now
                datePickerDialog.setTitle("");
                datePickerDialog.show();
                break;
            case R.id.cancelRide:
                Display display = this.getWindowManager().getDefaultDisplay();

                final Dialog dialog = new Dialog(this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setContentView(R.layout.default_popup);
                dialog.getWindow().setLayout(display.getWidth() * 90 / 100, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.setCancelable(false);

                TextView title = dialog.findViewById(R.id.title);
                TextView yes = dialog.findViewById(R.id.yes);
                TextView no = dialog.findViewById(R.id.no);
                yes.setText(getString(R.string.yes));
                title.setText(R.string.really_cancel);
                no.setVisibility(View.VISIBLE);

                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        cancelRide();
                    }
                });

                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                if (!dialog.isShowing()) {
                    dialog.show();
                }
                break;
            case R.id.sosAlert:
                double mylat = 0.0, mylon = 0.0;
                if (mLastLocation != null) {
                    mylat = mLastLocation.getLatitude();
                    mylon = mLastLocation.getLongitude();
                }
                Intent intent = new Intent(MapsActivity.this, SosAlert.class);
                intent.putExtra("lat", mylat);
                intent.putExtra("lon", mylon);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (paymentLay.getVisibility() == View.VISIBLE) {
            paymentLay.setVisibility(View.INVISIBLE);
            paymentLay.startAnimation(slidedown);
            bottomLay.setVisibility(View.VISIBLE);
            bottomLay.startAnimation(slideup);
            mMap.setPadding(0, 0, 0, bottomLay.getHeight());
        } else {
            super.onBackPressed();
        }
    }

    public void getRides(String distance) {
        HashMap<String, String> map = new HashMap<>();
        map.put("user_id", GetSet.getUserId());
        map.put("pickup_location", pickuppoint);
        map.put("pickup_lat", String.valueOf(pickuplat));
        map.put("pickup_lng", String.valueOf(pickuplng));
        map.put("drop_location", droppoint);
        map.put("drop_lat", String.valueOf(droplat));
        map.put("drop_lng", String.valueOf(droplng));
        map.put("distance", distance);
        Log.v("getRides params:", "" + map);
        Call<AvailableRide> call3 = apiInterface.grabRides(GetSet.getToken(), map);
        call3.enqueue(new Callback<AvailableRide>() {
            @Override
            public void onResponse(Call<AvailableRide> call, Response<AvailableRide> response) {
                try {
                    Log.v("response", "response=" + new Gson().toJson(response));
                    rideData = response.body();
                    if (rideData.status.equals("true")) {
                        availabeAry.addAll(rideData.result);
                        categoryAdapter.notifyDataSetChanged();
                        basePrice = availabeAry.get(selectedpos).baseprice;
                        approxprice.setText(getString(R.string.approxprice) + " : " + Constants.currency_symbol + " " + basePrice);
                        bottomLay.setVisibility(View.VISIBLE);
                        bottomLay.startAnimation(slideup);
                        mMap.setPadding(0, 0, 0, bottomLay.getHeight());
                    } else if (rideData.status.equals("false")) {
                        //  Toast.makeText(getApplicationContext(), rideData.message, Toast.LENGTH_SHORT).show();
                        bottomLay.setVisibility(View.GONE);
                        noServiceLay.setVisibility(View.VISIBLE);
                        noServiceLay.startAnimation(slideup);
                        mMap.setPadding(0, 0, 0, noServiceLay.getHeight());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<AvailableRide> call, Throwable t) {
                Log.v("Failed", "TEST");
                call.cancel();

            }
        });
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        Log.v("isConnected", "isConnected=" + isConnected);
        ApplicationClass.showSnack(this, findViewById(R.id.rootFrame), isConnected);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, ApplicationClass.intentFilter);
        if (NetworkReceiver.isConnected()) {
            ApplicationClass.showSnack(this, findViewById(R.id.rootFrame), true);
        } else {
            ApplicationClass.showSnack(this, findViewById(R.id.rootFrame), false);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
        ApplicationClass.showSnack(this, findViewById(R.id.rootFrame), true);
    }

    public void confirmRide() {
        HashMap<String, String> map = new HashMap<>();
        map.put("user_id", GetSet.getUserId());
        map.put("onride_id", onride_id);
     /*   map.put("type", type);
        map.put("payment_type", payment_type);
*/
        Log.v("confirmRide params:", "" + map);
        Call<ConfirmRideData> call3 = apiInterface.confirmRide(GetSet.getToken(), map);
        call3.enqueue(new Callback<ConfirmRideData>() {
            @Override
            public void onResponse(Call<ConfirmRideData> call, Response<ConfirmRideData> response) {
                try {
                    Log.v("response", "response=" + new Gson().toJson(response));
                    final ConfirmRideData rideData = response.body();
                    Handler handler = new Handler();
                    Runnable myRunnable = new Runnable() {
                        public void run() {
                            confirmRide();
                        }
                    };
                    if (rideData.status.equals("true")) {
                        handler.removeCallbacks(myRunnable);
                        removeAlldrivers();
                        if (from == null)
                            findingDriverDialog("rideAccepted");
                        showDriverDetail(rideData);
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("onride_id", onride_id);
                        mSocket.emit("whereareyou", jsonObject);
                    } else if (rideData.status.equals("false") && rideData.message.equals("Searching")) {
                        handler.postDelayed(myRunnable, 5000);
                        searchcount++;
                        if (searchcount == 10) {
                            handler.removeCallbacks(myRunnable);
                            if (dialog.isShowing())
                                dialog.dismiss();
                            nodriverLay.setVisibility(View.VISIBLE);
                            nodriverLay.startAnimation(slideup);
                            mMap.setPadding(0, 0, 0, nodriverLay.getHeight());
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ConfirmRideData> call, Throwable t) {
                Log.v("Failed", "TEST");
                call.cancel();
            }
        });
    }

    public void showDriverDetail(final ConfirmRideData rideData) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }

                //toolbar.setBackgroundColor(Color.WHITE);
                //statusView.setVisibility(View.VISIBLE);
                sosAlert.setVisibility(View.VISIBLE);
                driverDetailLay.setVisibility(View.VISIBLE);
                driverDetailLay.startAnimation(slideup);
                mMap.setPadding(0, 0, 0, driverDetailLay.getHeight());


                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.drawable.temp);
                requestOptions.error(R.drawable.temp);
                requestOptions.circleCropTransform();

                Glide.with(MapsActivity.this).load(rideData.driver_image).thumbnail(0.5f)
                        .apply(requestOptions)
                        .transition(new DrawableTransitionOptions().crossFade())
                        .into(driverImage);
                driverName.setText(rideData.driver_name);
                vehicleDetail.setText(rideData.driver_vehicle + "\n" + rideData.driver_vehicleno);
                otp.setText(getString(R.string.otp) + ": " + rideData.onride_otp);
                driverId.setText(rideData.driver_id);
                ratingCount.setText(rideData.driver_rating);
                approxAmount.setText(getString(R.string.approxprice) + " : " + Constants.currency_symbol + " " + basePrice);
                distance.setText(durationValue + "(" + distanceValue + ")");

                callDriver.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setData(Uri.parse("tel:" + rideData.driver_mobile));
                        startActivity(intent);
                    }
                });
            }
        }, 2000);
    }

    public void cancelRide() {
        HashMap<String, String> map = new HashMap<>();
        map.put("user_id", GetSet.getUserId());
        map.put("onride_id", onride_id);

        Log.v("cancelRide params:", "" + map);
        Call<Map<String, String>> call3 = apiInterface.cancelRide(GetSet.getToken(), map);
        call3.enqueue(new Callback<Map<String, String>>() {
            @Override
            public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                try {
                    Log.v("response", "response=" + new Gson().toJson(response));
                    Map<String, String> data = response.body();
                    if (data.get("status").equals("true")) {
                        finish();
                        Toast.makeText(getApplicationContext(), data.get("message"), Toast.LENGTH_SHORT).show();
                    } else if (data.get("status").equals("false")) {
                        Toast.makeText(getApplicationContext(), data.get("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Map<String, String>> call, Throwable t) {
                Log.v("Failed", "TEST");
                call.cancel();
            }
        });
    }

    public void requestRide() {
        HashMap<String, String> map = new HashMap<>();
        map.put("user_id", GetSet.getUserId());
        map.put("pickup_location", pickuppoint);
        map.put("pickup_lat", String.valueOf(pickuplat));
        map.put("pickup_lng", String.valueOf(pickuplng));
        map.put("drop_location", droppoint);
        map.put("drop_lat", String.valueOf(droplat));
        map.put("drop_lng", String.valueOf(droplng));
        map.put("category_id", availabeAry.get(selectedpos).id);
        map.put("type", type);
        map.put("payment_type", payment_type);
        map.put("baseprice", basePrice);
        if (type.equals("schedule")) {
            map.put("schedule_time", String.valueOf(timestamp));
        }

        Log.v("requestRide params:", "" + map);
        Call<RequestRideData> call3 = apiInterface.requestRide(GetSet.getToken(), map);
        call3.enqueue(new Callback<RequestRideData>() {
            @Override
            public void onResponse(Call<RequestRideData> call, Response<RequestRideData> response) {
                try {
                    Log.v("response", "response=" + new Gson().toJson(response));
                    RequestRideData rideData = response.body();
                    if (rideData.status.equals("true")) {
                        if (type.equals("schedule")) {
                            Toast.makeText(getApplicationContext(), getString(R.string.scheduled), Toast.LENGTH_SHORT).show();
                            Intent ride = new Intent(MapsActivity.this, RideActivity.class);
                            startActivity(ride);
                            finish();
                        } else {
                            onride_id = rideData.onride_id;
                            searchcount = 0;
                            confirmRide();
                        }
                    } else if (rideData.status.equals("false")) {
                        //  Toast.makeText(getApplicationContext(), rideData.message, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RequestRideData> call, Throwable t) {
                Log.v("Failed", "TEST");
                call.cancel();
            }
        });
    }

    public void findingDriverDialog(String from) {
        if (!dialog.isShowing()) {
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.finding_driver_dialog);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(false);

            dialog.show();
        }

        final LinearLayout findingDriver = dialog.findViewById(R.id.findingDriver);
        final LinearLayout rideAccepted = dialog.findViewById(R.id.rideAccepted);

        if (from.equals("findingDriver")) {
            findingDriver.setVisibility(View.VISIBLE);
        } else {
            rideAccepted.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        year = i;
        month = i1;
        day = i2;
        timePickerDialog.setTitle("");
        timePickerDialog.show();


    }


    @Override
    public void onTimeSet(TimePicker timePicker, int i, int i1) {

        Calendar c = Calendar.getInstance();
        //    c.setTimeZone(TimeZone.getTimeZone("GMT"));
        c.add(Calendar.MINUTE, 15);
        Log.v("day", "" + i + "," + i1);

        Calendar calendar = new GregorianCalendar(year, month, day, i, i1);
        long timestamp1 = calendar.getTimeInMillis() / 1000L;

        Calendar calendar2 = new GregorianCalendar(year, month, day, i, i1);
        calendar2.setTimeZone(TimeZone.getTimeZone("GMT"));
        timestamp = calendar2.getTimeInMillis() / 1000L;

        Log.v("currenttimestamp", "" + c.getTimeInMillis() / 1000L);
        Log.v("scheduletimestamp", "" + timestamp);

        if (timestamp1 > c.getTimeInMillis() / 1000L) {
            bottomLay.setVisibility(View.INVISIBLE);
            bottomLay.startAnimation(slidedown);
            paymentLay.setVisibility(View.VISIBLE);
            paymentLay.startAnimation(slideup);
        } else {
            Toast.makeText(this, getString(R.string.schedule_time_err), Toast.LENGTH_SHORT).show();
            timePickerDialog.setTitle("");
            timePickerDialog.show();
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSocket.off("iamhere");
        mSocket.off("cabNearBy");
        mSocket.disconnect();
        handler.removeCallbacks(cabneargo);
        //stop location updates when Activity is no longer active
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, MapsActivity.this);
        }
    }

    public class getDistancenDirection extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection mUrlConnection = null;
            StringBuilder mJsonResults = new StringBuilder();
            try {
                String distUrl = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + pickuplat + "," + pickuplng + "&destinations=" + droplat + "," + droplng + "&sensor=false&mode=driving";
                URL url = new URL(distUrl);
                mUrlConnection = (HttpURLConnection) url.openConnection();
                InputStreamReader in = new InputStreamReader(mUrlConnection.getInputStream());

                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    mJsonResults.append(buff, 0, read);
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            } finally {
                if (mUrlConnection != null) {
                    mUrlConnection.disconnect();
                }
            }

            try {
                JSONObject jsonObject = new JSONObject(mJsonResults.toString());
                JSONArray rows = jsonObject.optJSONArray("rows");
                if (rows != null) {
                    for (int i = 0; i < rows.length(); i++) {
                        JSONObject rowObj = rows.getJSONObject(i);
                        JSONArray elements = rowObj.getJSONArray("elements");
                        for (int j = 0; j < elements.length(); j++) {
                            JSONObject eleObj = elements.getJSONObject(j);
                            JSONObject distance = eleObj.getJSONObject("distance");
                            distanceValue = distance.getString("text");
                            JSONObject duration = eleObj.getJSONObject("duration");
                            durationValue = duration.getString("text");
                            break;
                        }
                        break;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return distanceValue;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            // String distance = response.replace(" km", "");
            if (from == null)
                getRides(response);
        }
    }

    private class DownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();
            parserTask.execute(result);

        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList();
                lineOptions = new PolylineOptions();

                List<HashMap<String, String>> path = result.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat").toString());
                    double lng = Double.parseDouble(point.get("lng").toString());
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }
                listLatLng.addAll(points);

                lineOptions.width(10);
                lineOptions.color(getResources().getColor(R.color.colorPrimary));
                lineOptions.startCap(new SquareCap());
                lineOptions.endCap(new SquareCap());
                lineOptions.jointType(ROUND);
                blackPolyLine = mMap.addPolyline(lineOptions);

                PolylineOptions greyOptions = new PolylineOptions();
                greyOptions.width(10);
                greyOptions.color(getResources().getColor(R.color.colorAccent));
                greyOptions.startCap(new SquareCap());
                greyOptions.endCap(new SquareCap());
                greyOptions.jointType(ROUND);
                greyPolyLine = mMap.addPolyline(greyOptions);
                animatePolyLine();

            }
        }
    }

    public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {
        ArrayList<AvailableRide.result> availableRides;
        Context context;

        public CategoryAdapter(Context context, ArrayList<AvailableRide.result> availableRides) {
            this.availableRides = availableRides;
            this.context = context;
        }

        @Override
        public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            CategoryItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.category_item, parent, false);
            return new CategoryViewHolder(binding);
        }

        @Override
        public void onBindViewHolder(CategoryViewHolder viewHolder, int position) {
            viewHolder.getBinding().name.setText(availableRides.get(position).category_name);
            viewHolder.getBinding().time.setText(availableRides.get(position).reach_pickup);
            Glide.with(MapsActivity.this).load(availableRides.get(position).category_image).thumbnail(0.5f)
                    .apply(circleCropTransform())
                    .transition(new DrawableTransitionOptions().crossFade())
                    .into(viewHolder.getBinding().image);

            if (position == selectedpos) {
                ColorMatrix colorMatrix = new ColorMatrix();
                colorMatrix.reset();
                ColorMatrixColorFilter filter = new ColorMatrixColorFilter(colorMatrix);
                viewHolder.getBinding().image.setColorFilter(filter);
                viewHolder.getBinding().image.startAnimation(animZoomin);
                viewHolder.getBinding().name.startAnimation(animZoomin);
                viewHolder.getBinding().time.startAnimation(animZoomin);
            } else {

                ColorMatrix colorMatrix = new ColorMatrix();
                colorMatrix.setSaturation(0);
                ColorMatrixColorFilter filter = new ColorMatrixColorFilter(colorMatrix);
                viewHolder.getBinding().image.setColorFilter(filter);
            }
        }

        @Override
        public int getItemCount() {
            return availableRides.size();
        }

        public class CategoryViewHolder extends RecyclerView.ViewHolder {
            CategoryItemBinding categoryItemLayBinding;

            public CategoryViewHolder(CategoryItemBinding layoutBinding) {
                super(layoutBinding.getRoot());
                categoryItemLayBinding = layoutBinding;
                categoryItemLayBinding.image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        removeAlldrivers();
                        handler.post(cabneargo);
                        selectedpos = getAdapterPosition();
                        basePrice = availabeAry.get(selectedpos).baseprice;
                        approxprice.setText(getString(R.string.approxprice) + " : " + Constants.currency_symbol + basePrice);
                        notifyDataSetChanged();
                    }
                });
            }

            public CategoryItemBinding getBinding() {
                return categoryItemLayBinding;
            }
        }
    }

    private void removeAlldrivers() {
        handler.removeCallbacks(cabneargo);

        for (int i = 0; i < markers.size(); i++) {
            markers.get(i).remove();
            markers.remove(i);
            driverid.remove(i);
        }


    }

    private Runnable cabneargo = new Runnable() {
        @Override
        public void run() {

            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("category_id", availabeAry.get(selectedpos).id);
                jsonObject.put("lat", String.valueOf(pickuplat));
                jsonObject.put("lng", String.valueOf(pickuplng));

                mSocket.emit("cabneargo", jsonObject);
                Log.v("Socket", "cabneargo Emit successfull");
                Log.v("Socket", jsonObject.toString());

            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d("Handlers", "Called on main thread");
            handler.postDelayed(cabneargo, 4000);
        }
    };

    public static Bitmap scaleBitmap(Bitmap bitmap, int newWidth, int newHeight) {
        Bitmap scaledBitmap = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888);

        float scaleX = newWidth / (float) bitmap.getWidth();
        float scaleY = newHeight / (float) bitmap.getHeight();
        float pivotX = 0;
        float pivotY = 0;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(scaleX, scaleY, pivotX, pivotY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bitmap, 0, 0, new Paint(Paint.FILTER_BITMAP_FLAG));

        return scaledBitmap;
    }
}
