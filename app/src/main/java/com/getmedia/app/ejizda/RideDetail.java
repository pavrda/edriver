package com.hitasoft.app.cabso;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.hitasoft.app.cabso.databinding.ActivityRideDetailBinding;
import com.hitasoft.app.utils.ApiClient;
import com.hitasoft.app.utils.ApiInterface;
import com.hitasoft.app.utils.Constants;
import com.hitasoft.app.utils.GetSet;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RideDetail extends AppCompatActivity implements View.OnClickListener {
    ActivityRideDetailBinding binding;
    Map<String, String> rideData;
    ApiInterface apiInterface;
    String from, onride_id, pickuptime, vehicleno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_detail);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_ride_detail);
        onride_id = getIntent().getStringExtra("onride_id");
        from = getIntent().getStringExtra("from");
        pickuptime = getIntent().getStringExtra("pickuptime");
        vehicleno = getIntent().getStringExtra("vehicleno");
        binding.title2.setText(ApplicationClass.getDateTimeFromStamp(Long.parseLong(pickuptime)));
        binding.toolbar.findViewById(R.id.backBtn).setVisibility(View.VISIBLE);
        binding.toolbar.findViewById(R.id.backBtn).setOnClickListener(this);
        binding.rideagain.setOnClickListener(this);
        binding.writeReview.setOnClickListener(this);
        binding.ratedLay.setOnClickListener(this);
        if (from.equals("completed"))
            completeRideDetail();
        else
            RideDetail();
    }

    public void setRidedata() {
        String url = "http://maps.google.com/maps/api/staticmap?center=" + rideData.get("pickup_lat") + "," + rideData.get("pickup_lng") + "&zoom=15&size=600x250&sensor=false";
        Glide.with(RideDetail.this).load(url).thumbnail(0.5f)
                .transition(new DrawableTransitionOptions().crossFade())
                .into(binding.mapView);

        binding.amount.setText(rideData.get("distance"));
        binding.pickupaddrs.setText(rideData.get("pickup_location"));
        binding.dropaddrs.setText(rideData.get("drop_location"));
        binding.cabType.setText(rideData.get("category_name"));

        if (from.equals("cancelled")) {
            Glide.with(this).load(rideData.get("category_image")).thumbnail(0.5f)
                    .apply(RequestOptions.circleCropTransform())
                    .transition(new DrawableTransitionOptions().crossFade())
                    .into(binding.cabImage);
            binding.subTitle.setText(R.string.cancelled);
            binding.canceledlay.setVisibility(View.VISIBLE);
            binding.secondlay.setVisibility(View.GONE);
            binding.writeReview.setVisibility(View.GONE);
            binding.distancelay.setVisibility(View.GONE);
            binding.driverDetailLay.setVisibility(View.GONE);
            binding.pickuptime.setText(ApplicationClass.getTimeFromStamp(Long.parseLong(pickuptime)));
            binding.droptime.setVisibility(View.GONE);
            binding.paymenttypetext.setText(getString(R.string.bill_details));
            binding.paymenttype.setText(getString(R.string.approxprice));
            binding.totalFare.setText(Constants.currency_symbol + " " + rideData.get("approx_price"));
        } else if (from.equals("scheduled") || from.equals("scheduleridenotaccepted")) {
            binding.cabImage.setImageDrawable(getResources().getDrawable(R.drawable.driver));
            binding.writeReview.setText(R.string.schedule);
            binding.writeReview.setOnClickListener(null);
            binding.writeReview.setVisibility(View.VISIBLE);
            binding.writeReview.setTextColor(getResources().getColor(R.color.green));
            binding.rideagain.setVisibility(View.VISIBLE);
            binding.rideagain.setText(R.string.cancel);
            binding.subTitle.setText(R.string.schedule);
            binding.secondlay.setVisibility(View.GONE);
            binding.distancelay.setVisibility(View.GONE);
            binding.driverDetailLay.setVisibility(View.GONE);
            binding.pickuptime.setText(ApplicationClass.getTimeFromStamp(Long.parseLong(pickuptime)));
            binding.droptime.setVisibility(View.GONE);
            binding.paymenttypetext.setText(getString(R.string.bill_details));
            binding.paymenttype.setText(getString(R.string.approxprice));
            binding.totalFare.setText(Constants.currency_symbol + " " + rideData.get("approx_price"));
        } else if (from.equals("completed")) {
            binding.subTitle.setText(rideData.get("vehicle_no"));
            binding.pickuptime.setText(ApplicationClass.getTimeFromStamp(Long.parseLong(rideData.get("pickup_time"))));
            binding.droptime.setText(ApplicationClass.getTimeFromStamp(Long.parseLong(rideData.get("drop_time"))));
            binding.driverName.setText(rideData.get("driver_name"));

            if (rideData.get("isreview").equals("true")) {
                binding.writeReview.setVisibility(View.GONE);
                binding.ratedLay.setVisibility(View.VISIBLE);
                binding.ratingBar.setRating(Float.valueOf(rideData.get("rating")));
            }
            if (rideData.get("ispayment").equals("false")) {
                binding.writeReview.setVisibility(View.GONE);
                binding.canceledlay.setVisibility(View.VISIBLE);
                binding.cancelimage.setVisibility(View.GONE);
                binding.canceltext.setText(getString(R.string.payment_pending));
                binding.rideagain.setVisibility(View.VISIBLE);
                binding.rideagain.setText(R.string.pay);
            } else {
                binding.rideagain.setVisibility(View.VISIBLE);
            }
            binding.tax.setText(Constants.currency_symbol + " " + rideData.get("ride_tax"));
            binding.rideFare.setText(Constants.currency_symbol + " " + rideData.get("ride_fare"));
            binding.totalBill.setText(Constants.currency_symbol + " " + rideData.get("ride_total"));
            binding.totalFare.setText(Constants.currency_symbol + " " + rideData.get("ride_total"));
            binding.paymenttype.setText(rideData.get("payment_type"));

            Glide.with(this).load(rideData.get("category_image")).thumbnail(0.5f)
                    .apply(RequestOptions.circleCropTransform())
                    .transition(new DrawableTransitionOptions().crossFade())
                    .into(binding.cabImage);

            Glide.with(this).load(rideData.get("driver_image")).thumbnail(0.5f)
                    .apply(RequestOptions.circleCropTransform())
                    .transition(new DrawableTransitionOptions().crossFade())
                    .into(binding.driverImage);
        }
    }


    public void RideDetail() {
        HashMap<String, String> map = new HashMap<>();
        map.put("user_id", GetSet.getUserId());
        map.put("onride_id", onride_id);
        map.put("type", from);

        Log.v("RideDetail params:", "" + map);
        Call<Map<String, String>> call3 = apiInterface.ridedetails(GetSet.getToken(), map);
        call3.enqueue(new Callback<Map<String, String>>() {
            @Override
            public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                try {
                    Log.v("RideDetail", "response=" + new Gson());
                    rideData = response.body();
                    if (rideData != null && !rideData.isEmpty()) {
                        if (rideData.get("status").equals("true")) {
                            setRidedata();
                        } else {
                            Toast.makeText(getApplicationContext(), rideData.get(Constants.TAG_MESSSAGE), Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Map<String, String>> call, Throwable t) {
                Log.v("Failed", "TEST");
                call.cancel();
            }
        });
    }

    public void completeRideDetail() {
        HashMap<String, String> map = new HashMap<>();
        map.put("user_id", GetSet.getUserId());
        map.put("onride_id", onride_id);

        Log.v("completeRide params:", "" + map);
        Call<Map<String, String>> call3 = apiInterface.completeridedetails(GetSet.getToken(), map);
        call3.enqueue(new Callback<Map<String, String>>() {
            @Override
            public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                try {
                    rideData = response.body();
                    Log.v("response", "response=" + rideData.toString());
                    if (rideData != null && !rideData.isEmpty()) {
                        if (rideData.get("status").equals("true")) {
                            setRidedata();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Map<String, String>> call, Throwable t) {
                Log.v("Failed", "TEST");
                call.cancel();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 5) {
            binding.writeReview.setVisibility(View.GONE);
            binding.ratedLay.setVisibility(View.VISIBLE);
            binding.ratingBar.setRating(Float.parseFloat(data.getStringExtra("rating")));
            rideData.put("rating", data.getStringExtra("rating"));
            rideData.put("review_message", data.getStringExtra("review_message"));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                onBackPressed();
                break;
            case R.id.writeReview:
                Intent ride = new Intent(RideDetail.this, WriteReview.class);
                ride.putExtra("onrideid", onride_id);
                startActivityForResult(ride, 5);
                break;
            case R.id.ratedLay:
                Intent rid = new Intent(RideDetail.this, WriteReview.class);
                rid.putExtra("onrideid", onride_id);
                rid.putExtra("rating", rideData.get("rating"));
                rid.putExtra("review_message", rideData.get("review_message"));
                startActivityForResult(rid, 5);
                break;
            case R.id.rideagain:
                if (binding.rideagain.getText().equals(getString(R.string.rideagain))) {
                    Intent b = new Intent(RideDetail.this, MapsActivity.class);
                    b.putExtra("pickuppoint", rideData.get("pickup_location"));
                    b.putExtra("pickuplat", Double.valueOf(rideData.get("pickup_lat")));
                    b.putExtra("pickuplng", Double.valueOf(rideData.get("pickup_lng")));
                    b.putExtra("droppoint", rideData.get("drop_location"));
                    b.putExtra("droplat", Double.valueOf(rideData.get("drop_lat")));
                    b.putExtra("droplng", Double.valueOf(rideData.get("drop_lng")));
                    startActivity(b);
                } else if (binding.rideagain.getText().equals(getString(R.string.cancel))) {
                    cancelRide();
                } else {
                    Intent i = new Intent(RideDetail.this, RideComplete.class);
                    i.putExtra("onride_id", onride_id);
                    startActivity(i);
                }
                break;
        }
    }

    public void cancelRide() {
        HashMap<String, String> map = new HashMap<>();
        map.put("user_id", GetSet.getUserId());
        map.put("onride_id", onride_id);

        Log.v("cancelRide params:", "" + map);
        Call<Map<String, String>> call3 = apiInterface.cancelRide(GetSet.getToken(), map);
        call3.enqueue(new Callback<Map<String, String>>() {
            @Override
            public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                try {
                    Log.v("response", "response=" + new Gson().toJson(response));
                    Map<String, String> data = response.body();
                    if (data.get("status").equals("true")) {
                        finish();
                        Toast.makeText(getApplicationContext(), data.get("message"), Toast.LENGTH_SHORT).show();
                    } else if (data.get("status").equals("false")) {
                        Toast.makeText(getApplicationContext(), data.get("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Map<String, String>> call, Throwable t) {
                Log.v("Failed", "TEST");
                call.cancel();
            }
        });
    }
}
