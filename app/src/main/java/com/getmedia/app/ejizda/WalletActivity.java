package com.hitasoft.app.cabso;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.google.android.gms.wallet.Cart;
import com.google.android.gms.wallet.LineItem;
import com.google.gson.Gson;
import com.hitasoft.app.helper.NetworkReceiver;
import com.hitasoft.app.utils.ApiClient;
import com.hitasoft.app.utils.ApiInterface;
import com.hitasoft.app.utils.Constants;
import com.hitasoft.app.utils.GetSet;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hitasoft on 22/4/18.
 */

public class WalletActivity extends AppCompatActivity implements NetworkReceiver.ConnectivityReceiverListener, View.OnClickListener {
    private static final int DROP_IN_REQUEST = 100;
    public static SharedPreferences pref;
    public static SharedPreferences.Editor editor;
    ImageView backBtn;
    TextView titleB, invite, moneyUsing, history, walletcurrency;
    EditText walletEdit;
    ApiInterface apiInterface;
    ProgressDialog dialog;
    NetworkReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);

        backBtn = findViewById(R.id.backBtn);
        titleB = findViewById(R.id.titleB);
        invite = findViewById(R.id.invite);
        walletEdit = findViewById(R.id.walletEdit);
        moneyUsing = findViewById(R.id.moneyUsing);
        history = findViewById(R.id.history);
        walletcurrency = findViewById(R.id.walletcurrency);
        pref = getApplicationContext().getSharedPreferences("SavedPref", MODE_PRIVATE);
        editor = pref.edit();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        backBtn.setVisibility(View.VISIBLE);
        history.setVisibility(View.VISIBLE);
        receiver = new NetworkReceiver();
        ApplicationClass.getInstance().setConnectivityListener(this);
        backBtn.setOnClickListener(this);
        invite.setOnClickListener(this);
        history.setOnClickListener(this);
        walletcurrency.setText(Constants.currency_symbol);

        dialog = new ProgressDialog(WalletActivity.this);
        dialog.setMessage(getString(R.string.pleasewait));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        String amt = GetSet.getWallet();
        if (amt == null) {
            amt = "0.00";
        }
        linkTextView(this, titleB, getString(R.string.balance), Constants.currency_symbol + amt, "title");
        linkTextView(this, moneyUsing, getString(R.string.money_using), getString(R.string.ridenow), "moneydes");
    }

    private void addWallet(String clientToken) {
        HashMap<String, String> map = new HashMap<>();
        map.put("user_id", GetSet.getUserId());
        map.put("amount", walletEdit.getText().toString());
        map.put("paynonce", clientToken);

        Log.v("map", "map=" + map);
        Call<Map<String, String>> call3 = apiInterface.addMoney(GetSet.getToken(), map);
        call3.enqueue(new Callback<Map<String, String>>() {
            @Override
            public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    Log.v("response", "response=" + new Gson().toJson(response));
                    Map<String, String> map = response.body();
                    if (map.get("status").equals("true")) {
                        walletEdit.setText("");
                        editor.putString("wallet", map.get("walletmoney"));
                        editor.commit();
                        GetSet.setWallet(map.get("walletmoney"));
                        linkTextView(WalletActivity.this, titleB, getString(R.string.balance), Constants.currency_symbol + map.get("walletmoney"), "title");
                        Toast.makeText(WalletActivity.this, map.get("message"), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(WalletActivity.this, map.get("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<Map<String, String>> call, Throwable t) {
                call.cancel();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void getClientToken() {
        HashMap<String, String> map = new HashMap<>();
        map.put("user_id", GetSet.getUserId());

        Call<Map<String, String>> call3 = apiInterface.getclienttoken(GetSet.getToken(), map);
        call3.enqueue(new Callback<Map<String, String>>() {
            @Override
            public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                try {
                    Log.v("response", "response=" + new Gson().toJson(response));
                    final Map<String, String> map = response.body();
                    if (map.get("status").equals("true")) {
                        DropInResult.fetchDropInResult(WalletActivity.this, map.get("clientToken"), new DropInResult.DropInResultListener() {
                            @Override
                            public void onError(Exception exception) {
                                // an error occurred
                                Log.v("TEST====", "onError");
                                Toast.makeText(WalletActivity.this, exception.getMessage(),
                                        Toast.LENGTH_SHORT).show();
                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                            }

                            @Override
                            public void onResult(DropInResult result) {
                                Cart cart = Cart.newBuilder()
                                        .setCurrencyCode("USD")
                                        .setTotalPrice(walletEdit.getText().toString())
                                        .addLineItem(LineItem.newBuilder()
                                                .setCurrencyCode("USD")
                                                .setDescription("Description")
                                                .setQuantity("1")
                                                .setUnitPrice(walletEdit.getText().toString())
                                                .setTotalPrice(walletEdit.getText().toString())
                                                .build())
                                        .build();

                                DropInRequest dropInRequest = new DropInRequest()
                                        .tokenizationKey(map.get("clientToken"))
                                        .amount(walletEdit.getText().toString())
                                        .androidPayCart(cart);
                                startActivityForResult(dropInRequest.getIntent(WalletActivity.this), DROP_IN_REQUEST);

                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                            }
                        });
                    } else {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        Toast.makeText(WalletActivity.this, map.get("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<Map<String, String>> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Toast.makeText(WalletActivity.this, getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();
                call.cancel();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == DROP_IN_REQUEST) {
            if (resultCode == RESULT_OK) {
                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                String paymentMethodNonce = result.getPaymentMethodNonce().getNonce();
                Log.v("paymentMethodNonce", "paymentMethodNonce=" + paymentMethodNonce);
                // send paymentMethodNonce to your server
                dialog.show();
                addWallet(paymentMethodNonce);
            } else if (resultCode != RESULT_CANCELED) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
                Toast.makeText(WalletActivity.this, ((Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR)).getMessage(),
                        Toast.LENGTH_SHORT).show();
            } else {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
                Toast.makeText(WalletActivity.this, getString(R.string.payment_cancelled),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void linkTextView(final Context context, TextView view, final String desc, final String appendTxt, final String from) {

        SpannableStringBuilder spanTxt = new SpannableStringBuilder(desc + " ");
        int count = spanTxt.length();
        spanTxt.append(appendTxt);
        spanTxt.setSpan(new MyClickableSpan(context, from) {
            @Override
            public void onClick(View widget) {
                if (from.equals("moneydes")) {
                    finish();
                }
            }
        }, count, spanTxt.length(), 0);

        view.setMovementMethod(LinkMovementMethod.getInstance());
        view.setText(spanTxt, TextView.BufferType.SPANNABLE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                finish();
                break;
            case R.id.invite:
                ApplicationClass.requestFocus(WalletActivity.this, walletEdit, false);
                if (walletEdit.getText().toString().trim().length() == 0) {
                    Toast.makeText(WalletActivity.this, getString(R.string.enter_money), Toast.LENGTH_SHORT).show();
                } else {
                    dialog.show();
                    getClientToken();
                }
                break;
            case R.id.history:
                Intent wallet = new Intent(this, WalletHistory.class);
                startActivity(wallet);
                break;
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        Log.v("isConnected", "isConnected=" + isConnected);
        ApplicationClass.showSnack(this, findViewById(R.id.activity_help), isConnected);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, ApplicationClass.intentFilter);
        if (NetworkReceiver.isConnected()) {
            ApplicationClass.showSnack(this, findViewById(R.id.activity_help), true);
        } else {
            ApplicationClass.showSnack(this, findViewById(R.id.activity_help), false);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
        ApplicationClass.showSnack(this, findViewById(R.id.activity_help), true);
    }

    class MyClickableSpan extends ClickableSpan {
        Context context;
        String from;

        public MyClickableSpan(Context context, String from) {
            super();
            this.context = context;
            this.from = from;
        }

        @Override
        public void onClick(View tv) {
            // FantacyApplication.showToast(context, name + ": " + id, Toast.LENGTH_SHORT);
        }

        @Override
        public void updateDrawState(TextPaint ds) {// override updateDrawState
            ds.setUnderlineText(false); // set to false to remove underline
            if (from.equals("moneydes")) {
                ds.setColor(ContextCompat.getColor(context, R.color.green));
            } else {
                ds.setColor(ContextCompat.getColor(context, R.color.red));
            }

        }
    }
}
