package com.hitasoft.app.cabso;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by hitasoft on 22/4/18.
 */

public class InviteActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView backBtn;
    TextView invite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite);

        backBtn = findViewById(R.id.backBtn);
        invite = findViewById(R.id.invite);

        backBtn.setVisibility(View.VISIBLE);

        backBtn.setOnClickListener(this);
        invite.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                finish();
                break;
            case R.id.invite:
                Intent g = new Intent(Intent.ACTION_SEND);
                g.setType("text/plain");
                g.putExtra(Intent.EXTRA_TEXT, getString(R.string.invite_message) + "https://play.google.com/store/apps/details?id=" +
                        getApplicationContext().getPackageName());
                startActivity(Intent.createChooser(g, "Share"));
                break;
        }
    }
}
