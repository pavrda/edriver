package com.hitasoft.app.cabso;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hitasoft.app.cabso.databinding.ActivityHelpBinding;
import com.hitasoft.app.cabso.databinding.HelpItemLayBinding;
import com.hitasoft.app.cabso.databinding.UsrContactLayoutBinding;
import com.hitasoft.app.model.HelpData;
import com.hitasoft.app.utils.ApiClient;
import com.hitasoft.app.utils.ApiInterface;
import com.hitasoft.app.utils.Constants;
import com.hitasoft.app.utils.GetSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hitasoft on 21/3/18.
 */

public class HelpActivity extends AppCompatActivity implements View.OnClickListener {
    static final String TAG = "HelpActivity";
    public static HelpData data = null;
    UsrContactLayoutBinding binding;
    ActivityHelpBinding helpBinding;
    BottomSheetDialog dialog;
    ImageView backBtn, nullImage;
    RelativeLayout nullLay, progress;
    TextView nullText;
    HelpAdapter helpAdapter;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    ApiInterface apiInterface;
    ArrayList<HelpData.Result> helpAry = new ArrayList<>();
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        helpBinding = DataBindingUtil.setContentView(this, R.layout.activity_help);

        backBtn = findViewById(R.id.backBtn);
        nullLay = findViewById(R.id.nullLay);
        nullText = findViewById(R.id.nullText);
        nullImage = findViewById(R.id.nullImage);
        progress = findViewById(R.id.progress);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.pleasewait));
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        helpBinding.helpList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        helpBinding.helpList.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        helpAdapter = new HelpAdapter(HelpActivity.this, helpAry);
        helpBinding.helpList.setAdapter(helpAdapter);
        helpAry.clear();

        nullImage.setImageResource(R.drawable.no_ride);
        nullText.setText(getString(R.string.no_data));
        progress.setVisibility(View.VISIBLE);

        getHelpData();

        backBtn.setVisibility(View.VISIBLE);

        backBtn.setOnClickListener(this);
        helpBinding.mailFab.setOnClickListener(this);
    }

    private void getHelpData() {
        Call<HelpData> call3 = apiInterface.getHelp();
        call3.enqueue(new Callback<HelpData>() {
            @Override
            public void onResponse(Call<HelpData> call, Response<HelpData> response) {
                progress.setVisibility(View.GONE);
                try {
                    data = response.body();
                    Log.v(TAG, "helpData=" + data.result);
                    if (data.status.equals("true")) {
                        helpAry.addAll(data.result);
                        helpAdapter.notifyDataSetChanged();
                    } else if (data.status.equals("false")) {

                    }
                    if (helpAry.size() == 0) {
                        nullLay.setVisibility(View.VISIBLE);
                    } else {
                        nullLay.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<HelpData> call, Throwable t) {
                call.cancel();
                progress.setVisibility(View.GONE);
                if (helpAry.size() == 0) {
                    nullLay.setVisibility(View.VISIBLE);
                } else {
                    nullLay.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                onBackPressed();
                break;
            case R.id.mail_fab:
                binding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.usr_contact_layout, null, false);
                dialog = new BottomSheetDialog(this);
                dialog.setContentView(binding.getRoot());
                dialog.setCanceledOnTouchOutside(false);
                dialog.setCancelable(false);
                dialog.show();

                /*TextInputLayout which displays additional padding once an error is displayed and isn't removed when the error is cleared or setErrorEnabled(false) is called.
                 * It can be solved by following code*/
                if (binding.usrnamelay.getChildCount() == 2)
                    binding.usrnamelay.getChildAt(1).setVisibility(View.GONE);
                if (binding.emaillay.getChildCount() == 2)
                    binding.emaillay.getChildAt(1).setVisibility(View.GONE);
                if (binding.subjectlay.getChildCount() == 2)
                    binding.subjectlay.getChildAt(1).setVisibility(View.GONE);
                if (binding.messagelay.getChildCount() == 2)
                    binding.messagelay.getChildAt(1).setVisibility(View.GONE);

                binding.cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        ApplicationClass.requestFocus(HelpActivity.this, binding.usrname, false);
                        ApplicationClass.requestFocus(HelpActivity.this, binding.email, false);
                        ApplicationClass.requestFocus(HelpActivity.this, binding.subject, false);
                        ApplicationClass.requestFocus(HelpActivity.this, binding.message, false);

                        dialog.dismiss();
                    }
                });
                binding.sendBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (binding.usrname.getText().toString().equals("")) {
                            binding.usrnamelay.getChildAt(1).setVisibility(View.VISIBLE);
                            binding.usrnamelay.setError(getString(R.string.name_error));
                            ApplicationClass.requestFocus(HelpActivity.this, binding.usrname, true);
                        } else if (binding.email.getText().toString().equals("")) {
                            binding.usrnamelay.setErrorEnabled(false);
                            binding.usrnamelay.getChildAt(1).setVisibility(View.GONE);
                            binding.emaillay.getChildAt(1).setVisibility(View.VISIBLE);
                            binding.emaillay.setError(getString(R.string.emailempty_error));
                            ApplicationClass.requestFocus(HelpActivity.this, binding.email, true);
                        } else if (!binding.email.getText().toString().matches(emailPattern)) {
                            binding.emaillay.setErrorEnabled(false);
                            binding.emaillay.getChildAt(1).setVisibility(View.GONE);
                            binding.emaillay.getChildAt(1).setVisibility(View.VISIBLE);
                            binding.emaillay.setError(getString(R.string.email_error));
                            ApplicationClass.requestFocus(HelpActivity.this, binding.email, true);
                        } else if (binding.subject.getText().toString().equals("")) {
                            binding.emaillay.setErrorEnabled(false);
                            binding.emaillay.getChildAt(1).setVisibility(View.GONE);
                            binding.subjectlay.getChildAt(1).setVisibility(View.VISIBLE);
                            binding.subjectlay.setError(getString(R.string.subject_error));
                            ApplicationClass.requestFocus(HelpActivity.this, binding.subject, true);
                        } else if (binding.message.getText().toString().equals("")) {
                            binding.subjectlay.setErrorEnabled(false);
                            binding.subjectlay.getChildAt(1).setVisibility(View.GONE);
                            binding.messagelay.getChildAt(1).setVisibility(View.VISIBLE);
                            binding.messagelay.setError(getString(R.string.message_error));
                            ApplicationClass.requestFocus(HelpActivity.this, binding.message, true);
                        } else {

                            contactus();
                        }
                    }
                });
                break;
        }
    }

    private void contactus() {
        progressDialog.show();
        Map<String, String> map = new HashMap<>();
        map.put(Constants.TAG_USER_ID, GetSet.getUserId());
        map.put("name", binding.usrname.getText().toString());
        map.put("email", binding.email.getText().toString());
        map.put("subject", binding.subject.getText().toString());
        map.put("message", binding.message.getText().toString());

        Log.v(TAG, "contactusParams:" + map);
        Call<Map<String, String>> call3 = apiInterface.contactus(GetSet.getToken(), map);
        call3.enqueue(new Callback<Map<String, String>>() {
            @Override
            public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                Map<String, String> data = response.body();
                Log.v(TAG, "contactusResponse=" + data);
                if (data.get(Constants.TAG_STATUS).equals("true")) {

                    if (dialog.isShowing())
                        dialog.dismiss();
                }
                Toast.makeText(HelpActivity.this, data.get(Constants.TAG_MESSSAGE), Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<Map<String, String>> call, Throwable t) {
                call.cancel();
                progressDialog.dismiss();
            }
        });
    }

    public class HelpAdapter extends RecyclerView.Adapter<HelpAdapter.HelpViewHolder> {
        ArrayList<HelpData.Result> helpTitleLists;
        Context context;

        public HelpAdapter(Context context, ArrayList<HelpData.Result> helpTitleLists) {
            this.helpTitleLists = helpTitleLists;
            this.context = context;
        }

        @Override
        public HelpViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            HelpItemLayBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.help_item_lay, parent, false);
            return new HelpViewHolder(binding);
        }

        @Override
        public void onBindViewHolder(HelpViewHolder viewHolder, int position) {
            Log.v(TAG, "helpList=" + helpTitleLists);
            viewHolder.getBinding().helpTitle.setText(helpTitleLists.get(position).title);
            viewHolder.getBinding().mainLay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Dialog helpdialog;
                    final ImageView dbackBtn;
                    helpdialog = new Dialog(HelpActivity.this, R.style.DialogStyle);
                    helpdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    helpdialog.setContentView(R.layout.help_dialog);

                    dbackBtn = helpdialog.findViewById(R.id.backBtn);
                    dbackBtn.setVisibility(View.VISIBLE);
                    helpdialog.setCancelable(true);

                    final WebView webview = helpdialog.findViewById(R.id.webView);
                    final TextView title = helpdialog.findViewById(R.id.title1);
                    title.setText(helpTitleLists.get(viewHolder.getAdapterPosition()).title);

                    String pish = "<html><head><style type=\"text/css\">@font-face {font-family: MyFont;src: url(\"file:///android_asset/font_regular.ttf\")}body {font-family: MyFont;font-size: medium;text-align: justify;}</style></head><body>";
                    String pas = "</body></html>";
                    String myHtmlString = pish + helpTitleLists.get(viewHolder.getAdapterPosition()).content + pas;
                    webview.loadDataWithBaseURL(null,myHtmlString, "text/html", "UTF-8", null);

                   // webview.loadData(, "text/html", "UTF-8");
                    dbackBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            helpdialog.dismiss();
                        }
                    });

                    if (!helpdialog.isShowing()) {
                        helpdialog.show();
                    }

                }
            });
        }

        @Override
        public int getItemCount() {
            return helpTitleLists.size();
        }

        public class HelpViewHolder extends RecyclerView.ViewHolder {
            HelpItemLayBinding helpItemLayBinding;

            public HelpViewHolder(HelpItemLayBinding layoutBinding) {
                super(layoutBinding.getRoot());
                helpItemLayBinding = layoutBinding;
            }

            public HelpItemLayBinding getBinding() {
                return helpItemLayBinding;
            }
        }
    }

}
