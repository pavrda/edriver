package com.hitasoft.app.cabso;

import android.app.Activity;
import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.multidex.MultiDex;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hitasoft.app.external.FontsOverride;
import com.hitasoft.app.helper.NetworkReceiver;
import com.hitasoft.app.utils.Constants;
import com.hitasoft.app.utils.GetSet;

import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;

import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import io.socket.client.IO;
import io.socket.client.Socket;

import static com.hitasoft.app.utils.Constants.CONNECTIVITY_ACTION;

/**
 * Created by hitasoft on 24/1/18.
 */

@ReportsCrashes(mailTo = "crashlog@hitasoft.com")
public class ApplicationClass extends Application {
    public static SharedPreferences pref;
    public static SharedPreferences.Editor editor;
    public static IntentFilter intentFilter;
    private static Snackbar snackbar = null;
    private static ApplicationClass mInstance;
    private static Toast toast = null;
    private Socket mSocket;

    public static synchronized ApplicationClass getInstance() {
        return mInstance;
    }

    // Showing network status in Snackbar
    public static void showSnack(final Context context, View view, boolean isConnected) {
        if (snackbar == null) {
            snackbar = Snackbar
                    .make(view, context.getString(R.string.network_failure), Snackbar.LENGTH_INDEFINITE)
                    .setAction("SETTINGS", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                            context.startActivity(intent);
                        }
                    });
            View sbView = snackbar.getView();
            TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
        }

        if (!isConnected && !snackbar.isShown()) {
            snackbar.show();
        } else {
            snackbar.dismiss();
            snackbar = null;
        }
    }

    public static void showToast(final Context context, String text, int duration) {

        if (toast == null || toast.getView().getWindowVisibility() != View.VISIBLE) {
            toast = Toast.makeText(context, text, duration);
            toast.show();
        } else toast.cancel();
    }

    /**
     * To convert the given dp value to pixel
     **/
    public static int dpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    public static void hideSoftKeyboard(Activity context, View view) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (NullPointerException npe) {
        } catch (Exception e) {
        }
    }

    public static float pxToDp(Context context, float px) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

    public static void requestFocus(Activity activity, View view, boolean isEnabled) {
        if (view.requestFocus()) {
            if (isEnabled)
                activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            else
                activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        }
    }

    public static void showStatusDialog(final Activity context, boolean status, String postedMessage, final Class destinationClass) {
        final Dialog dialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.default_dialog_lay);
        int height = ViewGroup.LayoutParams.MATCH_PARENT;
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setLayout(width, height);
        ImageView close = (ImageView) dialog.findViewById(R.id.close);
        TextView tick_posted = (TextView) dialog.findViewById(R.id.dialog_msg);
        tick_posted.setText(postedMessage);
        if (status) {
            dialog.show();
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.finish();
                    dialog.dismiss();
                }
            });
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(context, destinationClass);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(intent);
                    dialog.dismiss();
                }
            }, Constants.DIALOG_DELAY);
        }
    }

    public static String getDateTimeFromStamp(long timeStamp) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("EEEE", Locale.getDefault());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd yyyy' 'hh:mm a", Locale.getDefault());
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

            Date netDate = (new Date(timeStamp * 1000));
            String dayOfTheWeek = sdf.format(netDate);
            return dayOfTheWeek + " " + simpleDateFormat.format(netDate);
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }

    }

    public static String getTimeFromStamp(long timeStamp) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
            Date netDate = (new Date(timeStamp * 1000));
            return simpleDateFormat.format(netDate);
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }

    }

    public static String getDateTimeFromISO(String time) {
        try {
            DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'");
            Date value = df1.parse(time);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd yyyy' 'hh:mm a");
            return simpleDateFormat.format(value);
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        pref = getApplicationContext().getSharedPreferences("SavedPref", MODE_PRIVATE);
        editor = pref.edit();
        ACRA.init(this);
        intentFilter = new IntentFilter();
        intentFilter.addAction(CONNECTIVITY_ACTION);
        FontsOverride.setDefaultFont(this, "MONOSPACE", "font_regular.ttf");
        if (pref.getBoolean("isLogged", false)) {
            GetSet.setLogged(true);
            GetSet.setUserId(pref.getString("userId", null));
            GetSet.setUserName(pref.getString("userName", null));
            GetSet.setFullName(pref.getString("fullName", null));
            GetSet.setImageUrl(pref.getString("userImage", null));
            GetSet.setToken(pref.getString("token", null));
            GetSet.setWallet(pref.getString("wallet", null));
        }

        {
            try {
                mSocket = IO.socket(Constants.SOCKETURL);
            } catch (URISyntaxException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public Socket getSocket() {
        return mSocket;
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    public void setConnectivityListener(NetworkReceiver.ConnectivityReceiverListener listener) {
        NetworkReceiver.connectivityReceiverListener = listener;
    }
}
