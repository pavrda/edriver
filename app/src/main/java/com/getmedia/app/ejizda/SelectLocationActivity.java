package com.hitasoft.app.cabso;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.hitasoft.app.cabso.databinding.ActivitySelectLocationBinding;
import com.hitasoft.app.helper.PlaceAutocompleteAdapter;

import java.util.ArrayList;

public class SelectLocationActivity extends AppCompatActivity implements PlaceAutocompleteAdapter.PlaceAutoCompleteInterface, GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, View.OnClickListener {
    private static LatLngBounds BOUNDS = new LatLngBounds(new LatLng(-0, 0), new LatLng(0, 0));
    GoogleApiClient mGoogleApiClient;
    ActivitySelectLocationBinding binding;
    LinearLayoutManager llm;
    PlaceAutocompleteAdapter mAdapter;
    ImageView backBtn;
    String pickuppoint, currentpoint;
    Double pickuplat, pickuplng, currentlat, currentlng;
    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_location);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_select_location);
        pickuppoint = getIntent().getExtras().getString("pickuppoint");
        pickuplat = getIntent().getExtras().getDouble("pickuplat");
        pickuplng = getIntent().getExtras().getDouble("pickuplng");

        currentpoint = getIntent().getExtras().getString("pickuppoint");
        currentlat = getIntent().getExtras().getDouble("pickuplat");
        currentlng = getIntent().getExtras().getDouble("pickuplng");

        backBtn = findViewById(R.id.backBtn);
        binding.clearpickup.setVisibility(View.VISIBLE);

        backBtn.setVisibility(View.VISIBLE);
        backBtn.setOnClickListener(this);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0 /* clientId */, this)
                .addApi(Places.GEO_DATA_API)
                .build();
        initViews();
    }

    @Override
    public void onStart() {
        mGoogleApiClient.connect();
        super.onStart();

    }

    @Override
    public void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    /*
  Initialize Views
   */
    private void initViews() {
        mRecyclerView = (RecyclerView) findViewById(R.id.list_search);
        mRecyclerView.setHasFixedSize(true);
        llm = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(llm);
        binding.pickup.setText(pickuppoint);
        binding.drop.requestFocus();
        binding.clearpickup.setOnClickListener(this);
        binding.cleardrop.setOnClickListener(this);
        binding.locationlay.setOnClickListener(this);

        mAdapter = new PlaceAutocompleteAdapter(this, R.layout.view_placesearch,
                mGoogleApiClient, BOUNDS, null);
        changeBounds(pickuplat, pickuplng);
        mRecyclerView.setAdapter(mAdapter);

        binding.pickup.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {

                    binding.clearpickup.setVisibility(View.VISIBLE);
                    if (mAdapter != null) {
                        mRecyclerView.setAdapter(mAdapter);
                    }
                } else {
                    pickuppoint = "";
                    binding.clearpickup.setVisibility(View.GONE);
                }

                if (!s.toString().equals("") && mGoogleApiClient.isConnected()) {
                    mAdapter.getFilter().filter(s.toString());
                } else if (!mGoogleApiClient.isConnected()) {
//                    Toast.makeText(getApplicationContext(), Constants.API_NOT_CONNECTED, Toast.LENGTH_SHORT).show();
                    Log.e("", "NOT CONNECTED");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        binding.drop.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    binding.cleardrop.setVisibility(View.VISIBLE);
                    if (mAdapter != null) {
                        mRecyclerView.setAdapter(mAdapter);
                    }
                } else {
                    binding.cleardrop.setVisibility(View.GONE);
                }
                if (!s.toString().equals("") && mGoogleApiClient.isConnected()) {
                    mAdapter.getFilter().filter(s.toString());
                } else if (!mGoogleApiClient.isConnected()) {
//                    Toast.makeText(getApplicationContext(), Constants.API_NOT_CONNECTED, Toast.LENGTH_SHORT).show();
                    Log.e("", "NOT CONNECTED");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    void changeBounds(Double lat, Double lng) {
        LatLng northEast = new LatLng(lat, lng);
        LatLng southWest = new LatLng(lat, lng);
        BOUNDS = LatLngBounds.builder()
                .include(northEast)
                .include(southWest)
                .build();
        mAdapter.setBounds(BOUNDS);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                onBackPressed();
                break;
            case R.id.clearpickup:
                binding.pickup.setText("");
                break;
            case R.id.cleardrop:
                binding.drop.setText("");
                break;
            case R.id.locationlay:
                if (binding.pickup.isFocused()) {

                    pickuppoint = currentpoint;
                    pickuplat = currentlat;
                    pickuplng = currentlng;

                    binding.pickup.setText(pickuppoint);
                    changeBounds(pickuplat, pickuplng);
                } else if (pickuppoint.equals("")) {
                    Toast.makeText(getApplicationContext(), getString(R.string.pickuplocationerr), Toast.LENGTH_SHORT).show();

                } else {
                    Intent b = new Intent(SelectLocationActivity.this, MapsActivity.class);
                    b.putExtra("pickuppoint", pickuppoint);
                    b.putExtra("pickuplat", pickuplat);
                    b.putExtra("pickuplng", pickuplng);
                    b.putExtra("droppoint", currentpoint);
                    b.putExtra("droplat", currentlat);
                    b.putExtra("droplng", currentlng);
                    startActivity(b);
                    finish();
                }
                break;
        }
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onPlaceClick(ArrayList<PlaceAutocompleteAdapter.PlaceAutocomplete> mResultList, int position) {
        if (mResultList != null) {
            try {
                final String placeId = String.valueOf(mResultList.get(position).placeId);
                PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                        .getPlaceById(mGoogleApiClient, placeId);
                placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (places.getCount() == 1) {
                            if (binding.pickup.isFocused()) {

                                pickuppoint = places.get(0).getAddress().toString();
                                pickuplat = places.get(0).getLatLng().latitude;
                                pickuplng = places.get(0).getLatLng().longitude;

                                binding.pickup.setText(pickuppoint);
                                changeBounds(pickuplat, pickuplng);
                            } else if (pickuppoint.equals("")) {
                                Toast.makeText(getApplicationContext(), getString(R.string.pickuplocationerr), Toast.LENGTH_SHORT).show();

                            } else {
                                Intent b = new Intent(SelectLocationActivity.this, MapsActivity.class);
                                b.putExtra("pickuppoint", pickuppoint);
                                b.putExtra("pickuplat", pickuplat);
                                b.putExtra("pickuplng", pickuplng);
                                b.putExtra("droppoint", places.get(0).getAddress().toString());
                                b.putExtra("droplat", places.get(0).getLatLng().latitude);
                                b.putExtra("droplng", places.get(0).getLatLng().longitude);
                                startActivity(b);
                                finish();
                            }

                        } else {
                            Toast.makeText(getApplicationContext(), getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

             /*   final String placeId = String.valueOf(mResultList.get(position).placeId);


                PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                        .getPlaceById(mGoogleApiClient, placeId);
                placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (places.getCount() == 1) {
                            //Do the things here on Click.....
                         *//*   Intent data = new Intent();
                            data.putExtra("lat",String.valueOf(places.get(0).getLatLng().latitude));
                            data.putExtra("lng", String.valueOf(places.get(0).getLatLng().longitude));
                            setResult(SearchActivity.RESULT_OK, data);*//*
                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(), "something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
           */
            } catch (Exception e) {

            }

        }
    }

}
