package com.hitasoft.app.cabso;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.accountkit.AccessToken;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.hitasoft.app.cabso.databinding.ActivitySignupBinding;
import com.hitasoft.app.helper.NetworkReceiver;
import com.hitasoft.app.helper.SharedPrefManager;
import com.hitasoft.app.model.UserData;
import com.hitasoft.app.utils.ApiClient;
import com.hitasoft.app.utils.ApiInterface;
import com.hitasoft.app.utils.Constants;
import com.hitasoft.app.utils.GetSet;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener, NetworkReceiver.ConnectivityReceiverListener {
    private static final String TAG = "SignupActivity";
    public static int APP_REQUEST_CODE = 99;
    public static int RC_SIGN_IN = 101;
    public static SharedPreferences.Editor editor;
    public static SharedPreferences pref;
    static ApiInterface apiInterface;
    NetworkReceiver receiver;
    ProgressDialog progressDialog;
    GoogleSignInClient mGoogleSignInClient;
    ImageView backBtn;
    ActivitySignupBinding binding;
    String imageurl = "";
    CallbackManager callbackManager;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+", phoneNumber = "", countryCode = "";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == APP_REQUEST_CODE) {
            AccessToken accessToken = AccountKit.getCurrentAccessToken();
            if (accessToken != null) {
                //Handle Returning User
                AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
                    @Override
                    public void onSuccess(Account account) {
                        PhoneNumber phNumber = account.getPhoneNumber();
                        if (phNumber != null) {
                            phoneNumber = phNumber.getPhoneNumber();
                            countryCode = phNumber.getCountryCode();
                            signin();

                        }
                    }


                    @Override
                    public void onError(AccountKitError accountKitError) {
                        Toast.makeText(SignupActivity.this, getString(R.string.acc_created), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
            } else {
                Toast.makeText(SignupActivity.this, getString(R.string.acc_created), Toast.LENGTH_SHORT).show();
                finish();
            }
            if (progressDialog.isShowing())
                progressDialog.dismiss();

        } else if (requestCode == RC_SIGN_IN) {
            Log.v("GoogleSignInApi", "GoogleSignInApi");
            @SuppressLint("RestrictedApi")
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            if (progressDialog.isShowing())
                progressDialog.dismiss();
            handleSignInResult(task);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_signup);
        backBtn = findViewById(R.id.backBtn);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        receiver = new NetworkReceiver();
        ApplicationClass.getInstance().setConnectivityListener(this);
        pref = getApplicationContext().getSharedPreferences("SavedPref", MODE_PRIVATE);
        editor = pref.edit();
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.pleasewait));
        progressDialog.setCancelable(false);

        backBtn.setVisibility(View.VISIBLE);
        backBtn.setOnClickListener(this);
        binding.signin.setOnClickListener(this);
        binding.next.setOnClickListener(this);
        binding.fbLay.setOnClickListener(this);
        binding.gplusLay.setOnClickListener(this);
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    private void signup() {
        progressDialog.show();

        Map<String, String> map = new HashMap<>();
        map.put(Constants.TAG_FULL_NAME, binding.name.getText().toString());
        map.put(Constants.TAG_EMAIL, binding.email.getText().toString());
        map.put(Constants.TAG_PASSWORD, binding.password.getText().toString());

        if (!imageurl.equals(""))
            map.put(Constants.TAG_IMAGE_URL, imageurl);


        Log.v("MapMap", "SIGNUP" + map);
        Call<UserData> call3 = apiInterface.signup(map);
        call3.enqueue(new Callback<UserData>() {
            @Override
            public void onResponse(Call<UserData> call, Response<UserData> response) {
                try {
                    UserData data = response.body();
                    if (data.status.equals("true")) {
                        verifyMobileNo();
                    } else if (data.status.equals("false")) {

                        Toast.makeText(SignupActivity.this, data.message, Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<UserData> call, Throwable t) {
                call.cancel();
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }

    public void signin() {
        progressDialog.show();
        HashMap<String, String> map = new HashMap<>();
        map.put(Constants.TAG_EMAIL, binding.email.getText().toString());
        map.put(Constants.TAG_PASSWORD, binding.password.getText().toString());
        map.put(Constants.TAG_PHONE_NUMBER, phoneNumber);
        map.put(Constants.TAG_COUNTRY_CODE, countryCode);
        Log.v("Login params:", "" + map);
        Call<UserData> call3 = apiInterface.signin(map);
        call3.enqueue(new Callback<UserData>() {
            @Override
            public void onResponse(Call<UserData> call, Response<UserData> response) {
                try {
                    UserData userdata = response.body();

                    if (userdata.status.equals("true")) {

                        GetSet.Logingin(userdata.user_id, userdata.full_name, userdata.imageurl, userdata.token, userdata.walletmoney);

                        editor.putBoolean("isLogged", true);
                        editor.putString("userId", userdata.user_id);
                        editor.putString("userImage", userdata.imageurl);
                        editor.putString("fullName", userdata.full_name);
                        editor.putString("token", userdata.token);
                        editor.putString("password", userdata.password);
                        editor.commit();
                        GetSet.setPassword(userdata.password);
                        GetSet.setToken(userdata.token);
                        addDeviceId(SignupActivity.this);

                    } else if (userdata.status.equals("false")) {
                        Toast.makeText(getApplicationContext(), userdata.message, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<UserData> call, Throwable t) {
                Log.v("Login Failed", "TEST");
                call.cancel();
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }

    private void addDeviceId(final Context context) {

        final String token = SharedPrefManager.getInstance(context).getDeviceToken();
        final String deviceId = android.provider.Settings.Secure.getString(context.getContentResolver(),
                android.provider.Settings.Secure.ANDROID_ID);

        Map<String, String> map = new HashMap<>();
        map.put("user_id", GetSet.getUserId());
        map.put("device_token", token);
        map.put("device_type", "1");
        map.put("device_id", deviceId);

        Log.v("addDeviceId:", "Params- " + map);
        Call<Map<String, String>> call3 = apiInterface.pushsignin(GetSet.getToken(), map);
        call3.enqueue(new Callback<Map<String, String>>() {
            @Override
            public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                Map<String, String> data = response.body();
                Log.v("addDeviceId:", "response- " + data);
                Intent b = new Intent(SignupActivity.this, MainActivity.class);
                startActivity(b);
            }

            @Override
            public void onFailure(Call<Map<String, String>> call, Throwable t) {
                call.cancel();
            }
        });
        progressDialog.dismiss();
    }


    public void verifyMobileNo() {
        final Intent intent = new Intent(SignupActivity.this, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        LoginType.PHONE,
                        AccountKitActivity.ResponseType.TOKEN);
        configurationBuilder.setReadPhoneStateEnabled(true);
        configurationBuilder.setReceiveSMS(true);
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        startActivityForResult(intent, APP_REQUEST_CODE);
    }

    public void initFacebook() {

        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject profile, GraphResponse response) {
                                        Log.v("json", "object" + profile);
                                        // Application code
                                        try {
                                            if (profile.has("email")) {
                                                URL image_value = null;
                                                try {
                                                    final String userId = profile.getString("id");
                                                    image_value = new URL("http://graph.facebook.com/" + userId + "/picture?type=large");
                                                    imageurl = image_value.toString();
                                                } catch (MalformedURLException e) {
                                                    e.printStackTrace();
                                                }
                                                binding.name.setText(profile.getString("name"));
                                                binding.email.setText(profile.getString("email"));
                                                binding.repassword.setText("");
                                                binding.password.setText("");
                                            } else {
                                                ApplicationClass.showToast(SignupActivity.this, "Facebook account must have Email to login", Toast.LENGTH_SHORT);
                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,first_name,last_name");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {

                        ApplicationClass.showToast(SignupActivity.this, "Facebook - Cancelled", Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        ApplicationClass.showToast(SignupActivity.this, "Facebook - " + exception.getMessage(), Toast.LENGTH_SHORT);
                        if (exception instanceof FacebookAuthorizationException) {
                            if (com.facebook.AccessToken.getCurrentAccessToken() != null) {
                                LoginManager.getInstance().logOut();
                            }
                        }
                    }
                });

    }

    @SuppressLint("RestrictedApi")
    public void initGplus() {
        progressDialog.show();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount acct = completedTask.getResult(ApiException.class);
            if (acct.getPhotoUrl() == null) {
                imageurl = "";
            } else {
                imageurl = acct.getPhotoUrl().toString();
            }
            binding.name.setText(acct.getDisplayName());
            binding.email.setText(acct.getEmail());
            binding.repassword.setText("");
            binding.password.setText("");
            progressDialog.dismiss();
        } catch (ApiException e) {
            e.printStackTrace();
        }
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                onBackPressed();
                break;
            case R.id.signin:
                Intent b = new Intent(SignupActivity.this, LoginActivity.class);
                startActivity(b);
                break;
            case R.id.next:
                binding.passwordlay.setError(null);
                binding.emaillay.setError(null);
                binding.namelay.setError(null);
                binding.repasswordlay.setError(null);
                if (binding.name.getText().toString().equals("")) {
                    binding.namelay.setError(getString(R.string.name_error));
                    requestFocus(binding.name);
                } else if (binding.name.getText().toString().length() < 3) {
                    binding.namelay.setError(getString(R.string.fullname_error));
                    requestFocus(binding.name);
                } else if (binding.email.getText().toString().equals("")) {
                    binding.emaillay.setError(getString(R.string.emailempty_error));
                    requestFocus(binding.email);
                } else if (!binding.email.getText().toString().matches(emailPattern)) {
                    binding.emaillay.setError(getString(R.string.email_error));
                    requestFocus(binding.email);
                } else if (binding.password.getText().toString().equals("")) {
                    binding.passwordlay.setError(getString(R.string.passwordempty_error));
                    requestFocus(binding.password);
                } else if (binding.password.getText().toString().length() < 6) {
                    binding.passwordlay.setError(getString(R.string.password_error));
                    requestFocus(binding.password);
                } else if (!binding.repassword.getText().toString().equals(binding.password.getText().toString())) {
                    binding.repasswordlay.setError(getString(R.string.repassword_error));
                    requestFocus(binding.repassword);
                } else {
                    signup();

                }

                break;
            case R.id.fbLay:
                initFacebook();
                LoginManager.getInstance().logInWithReadPermissions(SignupActivity.this, Arrays.asList("public_profile", "email"));
                break;
            case R.id.gplusLay:
                initGplus();
                @SuppressLint("RestrictedApi")
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                SignupActivity.this.startActivityForResult(signInIntent, RC_SIGN_IN);
                break;
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        Log.v("isConnected", "isConnected=" + isConnected);
        ApplicationClass.showSnack(this, findViewById(R.id.activity_signup), isConnected);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, ApplicationClass.intentFilter);
        if (NetworkReceiver.isConnected()) {
            ApplicationClass.showSnack(this, findViewById(R.id.activity_signup), true);
        } else {
            ApplicationClass.showSnack(this, findViewById(R.id.activity_signup), false);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
        ApplicationClass.showSnack(this, findViewById(R.id.activity_signup), true);
    }


}
