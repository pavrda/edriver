package com.hitasoft.app.cabso;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hitasoft.app.utils.ApiClient;
import com.hitasoft.app.utils.ApiInterface;
import com.hitasoft.app.utils.GetSet;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hitasoft on 22/4/18.
 */

public class WriteReview extends AppCompatActivity implements View.OnClickListener {

    ImageView backBtn;
    RatingBar ratingBar;
    TextView submit;
    EditText reviewMsg;
    ApiInterface apiInterface;
    String onrideid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.write_review);

        backBtn = findViewById(R.id.backBtn);
        ratingBar = findViewById(R.id.ratingBar);
        submit = findViewById(R.id.submit);
        reviewMsg = findViewById(R.id.reviewMsg);

        backBtn.setVisibility(View.VISIBLE);
        backBtn.setImageResource(R.drawable.close);
        onrideid = getIntent().getExtras().getString("onrideid");
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable().getCurrent();
        stars.getDrawable(2).setColorFilter(getResources().getColor(R.color.notification), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(0).setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(2).setColorFilter(getResources().getColor(R.color.notification), PorterDuff.Mode.SRC_ATOP);

        backBtn.setOnClickListener(this);
        submit.setOnClickListener(this);

        String rating = getIntent().getStringExtra("rating");
        String msg = getIntent().getStringExtra("review_message");
        if (rating != null) {
            ratingBar.setRating(Float.parseFloat(rating));
            reviewMsg.setText(msg);
        }
    }

    private void setReview() {
        HashMap<String, String> map = new HashMap<>();
        map.put("user_id", GetSet.getUserId());
        map.put("onride_id", onrideid);
        map.put("review_message", reviewMsg.getText().toString());
        map.put("rating", String.valueOf(ratingBar.getRating()));
        Call<Map<String, String>> call3 = apiInterface.review(GetSet.getToken(), map);
        call3.enqueue(new Callback<Map<String, String>>() {
            @Override
            public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                try {
                    Map<String, String> map = response.body();
                    if (map.get("status").equals("true")) {
                        Toast.makeText(WriteReview.this, map.get("message"), Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(WriteReview.this, RideDetail.class);
                        i.putExtra("rating", String.valueOf(ratingBar.getRating()));
                        i.putExtra("review_message", reviewMsg.getText().toString());
                        setResult(RESULT_OK, i);
                        finish();
                    } else {
                        Toast.makeText(WriteReview.this, map.get("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<Map<String, String>> call, Throwable t) {
                call.cancel();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                finish();
                break;
            case R.id.submit:
                if (ratingBar.getRating() < 0.5) {
                    Toast.makeText(WriteReview.this, "Kindly Rate the driver!", Toast.LENGTH_SHORT).show();
                } else
                    setReview();
                break;
        }
    }
}
