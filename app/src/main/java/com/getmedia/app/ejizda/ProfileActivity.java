package com.hitasoft.app.cabso;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.accountkit.AccessToken;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.google.gson.Gson;
import com.hitasoft.app.cabso.databinding.ActivityProfileBinding;
import com.hitasoft.app.cabso.databinding.NameDialogBinding;
import com.hitasoft.app.cabso.databinding.PasswordUpdateLayoutBinding;
import com.hitasoft.app.external.ImagePicker;
import com.hitasoft.app.helper.NetworkReceiver;
import com.hitasoft.app.model.ProfileData;
import com.hitasoft.app.utils.ApiClient;
import com.hitasoft.app.utils.ApiInterface;
import com.hitasoft.app.utils.Constants;
import com.hitasoft.app.utils.GetSet;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by hitasoft on 20/3/18.
 */

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener, NetworkReceiver.ConnectivityReceiverListener {

    static final String TAG = "ProfileActivity";
    public static int APP_REQUEST_CODE = 99;
    public static ProfileData data = null;
    public static ArrayList<ProfileData.EmergencyContact> emergencyList = new ArrayList<>();
    static ApiInterface apiInterface;
    ActivityProfileBinding profBinding;
    ProgressDialog progressDialog;
    ImageView backBtn;
    BottomSheetDialog dialog;
    BottomSheetDialog forgotdialog;
    Display display;
    String phoneNumber = "", countryCode = "";
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    NetworkReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        profBinding = DataBindingUtil.setContentView(this, R.layout.activity_profile);

        backBtn = findViewById(R.id.backBtn);
        receiver = new NetworkReceiver();
        ApplicationClass.getInstance().setConnectivityListener(this);
        backBtn.setVisibility(View.VISIBLE);
        backBtn.setOnClickListener(this);
        pref = ProfileActivity.this.getSharedPreferences("SavedPref", MODE_PRIVATE);
        editor = pref.edit();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.pleasewait));
        progressDialog.setCancelable(false);

        if (data != null) {
            setProfileData();
        } else {
            profBinding.progress.setVisibility(View.VISIBLE);
            profBinding.profLay.setVisibility(View.GONE);
        }
        getProfileData();

        profBinding.usrLogout.setOnClickListener(this);
        profBinding.profPassword.setOnClickListener(this);
        profBinding.userImage.setOnClickListener(this);
        profBinding.editUsr.setOnClickListener(this);
        profBinding.sosalert.setOnClickListener(this);

        profBinding.profPhoneno.setOnClickListener(this);
        profBinding.profName.setOnClickListener(this);
    }

    public byte[] getBytes(InputStream is) throws IOException {
        ByteArrayOutputStream byteBuff = new ByteArrayOutputStream();
        int buffSize = 1024;
        byte[] buff = new byte[buffSize];
        int len = 0;
        while ((len = is.read(buff)) != -1) {
            byteBuff.write(buff, 0, len);
        }
        return byteBuff.toByteArray();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1 && requestCode == 234) {
            try {
                InputStream is = getContentResolver().openInputStream(data.getData());
                uploadImage(getBytes(is));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (requestCode == APP_REQUEST_CODE) {
            AccessToken accessToken = AccountKit.getCurrentAccessToken();
            if (accessToken != null) {
                //Handle Returning User
                AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
                    @Override
                    public void onSuccess(Account account) {
                        PhoneNumber phNumber = account.getPhoneNumber();
                        if (phNumber != null) {
                            phoneNumber = phNumber.getPhoneNumber();
                            countryCode = phNumber.getCountryCode();
                            progressDialog.show();
                            changePhoneNumber(phoneNumber, countryCode);
                        }
                    }

                    @Override
                    public void onError(AccountKitError accountKitError) {

                    }
                });
            }

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 100:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    ImagePicker.pickImage(this, "Select your image:");
                }
                break;
        }
    }

    /*API Integration*/

    /**
     * Function to get an User Profile Datas
     */

    private void getProfileData() {
        Call<ProfileData> call3 = apiInterface.profile(GetSet.getToken(), GetSet.getUserId());
        Log.v(TAG, "token=" + GetSet.getToken());
        Log.v(TAG, "userId=" + GetSet.getUserId());
        call3.enqueue(new Callback<ProfileData>() {
            @Override
            public void onResponse(Call<ProfileData> call, Response<ProfileData> response) {
                try {
                    data = response.body();
                    Log.v("response", "response=" + new Gson().toJson(response));
                    if (data.status.equals("true")) {
                        if (emergencyList != null)
                            emergencyList.clear();
                        if (data.status.equals("true")) {
                            emergencyList.addAll(data.emergency_contact);
                            editor.putString("wallet", data.walletmoney);
                            editor.commit();
                            GetSet.setWallet(data.walletmoney);
                            setProfileData();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ProfileData> call, Throwable t) {
                call.cancel();
            }
        });
    }

    /**
     * Function to Upload an User Image to Server
     */

    private void uploadImage(byte[] imageBytes) {
        progressDialog.show();
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), imageBytes);
        MultipartBody.Part body = MultipartBody.Part.createFormData("userImage", "image.jpg", requestFile);

        RequestBody userid = RequestBody.create(MediaType.parse("multipart/form-data"), GetSet.getUserId());
        Call<Map<String, String>> call3 = apiInterface.uploadProfileImage(GetSet.getToken(), body, userid);
        call3.enqueue(new Callback<Map<String, String>>() {
            @Override
            public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                Map<String, String> data = response.body();
                Log.v(TAG, "uploadImageresponse=" + data);

                if (data.get(Constants.TAG_USER_IMAGE) != null) {
                    GetSet.setImageUrl(data.get(Constants.TAG_USER_IMAGE));
                    editor.putString("userImage", data.get(Constants.TAG_USER_IMAGE));
                    editor.commit();
                    Glide.with(ProfileActivity.this).load(data.get(Constants.TAG_USER_IMAGE)).thumbnail(0.5f)
                            .apply(RequestOptions.circleCropTransform())
                            .transition(new DrawableTransitionOptions().crossFade())
                            .into(profBinding.userImage);
                    Glide.with(ProfileActivity.this).load(data.get(Constants.TAG_USER_IMAGE)).thumbnail(0.5f)
                            .apply(RequestOptions.circleCropTransform())
                            .transition(new DrawableTransitionOptions().crossFade())
                            .into(MainActivity.userImage);
                }
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<Map<String, String>> call, Throwable t) {
                Log.v(TAG, "onFailure=" + "onFailure");
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                call.cancel();
            }
        });
    }

    /**
     * Function to Change an User Password
     */

    private void changePassword(String password, final Dialog dialog, final PasswordUpdateLayoutBinding binding) {
        Map<String, String> map = new HashMap<>();
        map.put(Constants.TAG_USER_ID, GetSet.getUserId());
        map.put(Constants.TAG_NEW_PASSWORD, password);

        Log.v(TAG, "changePasswordParams:" + map);
        Call<Map<String, String>> call3 = apiInterface.changePassword(GetSet.getToken(), map);
        call3.enqueue(new Callback<Map<String, String>>() {
            @Override
            public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                Map<String, String> data = response.body();
                Log.v(TAG, "changePasswordResponse=" + data);
                if (data.get(Constants.TAG_STATUS).equals("true")) {
                    if (dialog.isShowing())
                        dialog.dismiss();
                    GetSet.setPassword(binding.reEnterPswd.getText().toString());
                    editor.putString("password", binding.reEnterPswd.getText().toString());
                    editor.commit();
                    profBinding.profPassword.setText(binding.reEnterPswd.getText().toString());
                }
                Toast.makeText(ProfileActivity.this, data.get(Constants.TAG_MESSSAGE), Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<Map<String, String>> call, Throwable t) {
                call.cancel();
            }
        });
    }

    /*Set Data From API*/

    private void setProfileData() {
        if (data.status.equals("true")) {
            profBinding.profName.setText(data.full_name);
            profBinding.profEmail.setText(data.email);
            profBinding.profPassword.setText(GetSet.getPassword());
            profBinding.profPhoneno.setText(data.phone_number);
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.temp);
            requestOptions.error(R.drawable.temp);
            requestOptions.circleCropTransform();
            if (data.user_image != null)
                Glide.with(this).load(data.user_image).thumbnail(0.5f).apply(requestOptions).into(profBinding.userImage);
            profBinding.progress.setVisibility(View.GONE);
            profBinding.profLay.setVisibility(View.VISIBLE);

        } else {
        }
    }

    private void removeToken() {

        final String deviceId = android.provider.Settings.Secure.getString(getApplicationContext().getContentResolver(),
                android.provider.Settings.Secure.ANDROID_ID);
        Map<String, String> map = new HashMap<>();
        map.put("user_id", GetSet.getUserId());
        map.put("device_id", deviceId);
        Log.v("removeToken:", "Params- " + map);
        Call<Map<String, String>> call3 = apiInterface.pushsignout(GetSet.getToken(), map);
        call3.enqueue(new Callback<Map<String, String>>() {
            @Override
            public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                Map<String, String> data = response.body();
                Log.v("addDeviceId:", "response- " + data);

            }

            @Override
            public void onFailure(Call<Map<String, String>> call, Throwable t) {
                call.cancel();
            }
        });
    }

    public void verifyMobileNo(View v) {
        final Intent intent = new Intent(ProfileActivity.this, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        LoginType.PHONE,
                        AccountKitActivity.ResponseType.TOKEN);
        configurationBuilder.setReadPhoneStateEnabled(true);
        configurationBuilder.setReceiveSMS(true);
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        startActivityForResult(intent, APP_REQUEST_CODE);
    }

    /**
     * Function for Onclick Event
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        Log.v("isConnected", "isConnected=" + isConnected);
        ApplicationClass.showSnack(this, findViewById(R.id.activity_profile), isConnected);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, ApplicationClass.intentFilter);
        if (NetworkReceiver.isConnected()) {
            ApplicationClass.showSnack(this, findViewById(R.id.activity_profile), true);
        } else {
            ApplicationClass.showSnack(this, findViewById(R.id.activity_profile), false);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
        ApplicationClass.showSnack(this, findViewById(R.id.activity_profile), true);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                onBackPressed();
                break;
            case R.id.profPassword:
                final PasswordUpdateLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.password_update_layout, null, false);
                dialog = new BottomSheetDialog(this);
                dialog.setContentView(binding.getRoot());
                dialog.setCanceledOnTouchOutside(false);
                dialog.setCancelable(false);
                dialog.show();
                binding.oldPswdlay.setError(null);
                binding.newPswdlay.setError(null);
                binding.reEnterPswdlay.setError(null);

                /*Dialog Button Listeners*/
                binding.cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ApplicationClass.hideSoftKeyboard(ProfileActivity.this, view);
                        dialog.dismiss();
                    }
                });
                binding.saveBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        binding.oldPswdlay.setError(null);
                        binding.newPswdlay.setError(null);
                        binding.reEnterPswdlay.setError(null);
                        if (binding.oldPswd.getText().toString().equals("")) {
                            binding.oldPswdlay.setError(getString(R.string.em_oldpswd_error));
                            ApplicationClass.requestFocus(ProfileActivity.this, binding.oldPswd, true);
                        } else if (!binding.oldPswd.getText().toString().equals(GetSet.getPassword())) {
                            binding.oldPswdlay.setError(getString(R.string.em_oldpswd_mismatch_error));
                            ApplicationClass.requestFocus(ProfileActivity.this, binding.newPswd, true);
                        } else if (binding.newPswd.getText().toString().equals("")) {
                            binding.newPswdlay.setError(getString(R.string.em_newpswd_error));
                            ApplicationClass.requestFocus(ProfileActivity.this, binding.newPswd, true);
                        } else if (binding.newPswd.getText().toString().length() < 6) {
                            binding.newPswd.setError(getString(R.string.password_error));
                            ApplicationClass.requestFocus(ProfileActivity.this, binding.newPswd, true);
                        } else if (binding.reEnterPswd.getText().toString().equals("")) {
                            binding.reEnterPswdlay.setError(getString(R.string.em_reenpswd_error));
                            ApplicationClass.requestFocus(ProfileActivity.this, binding.reEnterPswd, true);
                        } else if (!binding.newPswd.getText().toString().equals(binding.reEnterPswd.getText().toString())) {
                            binding.reEnterPswdlay.setError(getString(R.string.repassword_error));
                            ApplicationClass.requestFocus(ProfileActivity.this, binding.reEnterPswd, true);
                        } else {
                            dialog.dismiss();
                            binding.oldPswdlay.setErrorEnabled(false);
                            profBinding.profPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                            changePassword(binding.reEnterPswd.getText().toString(), dialog, binding);
                        }
                    }
                });
                break;
            case R.id.userImage:
            case R.id.edit_usr:
                if (ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE}, 100);
                } else {
                    ImagePicker.pickImage(this, "Select your image:");
                }
                break;
            case R.id.sosalert:
                Intent intent = new Intent(this, EmergencyContactActivity.class);
                startActivity(intent);
                break;
            case R.id.usrLogout:
                display = this.getWindowManager().getDefaultDisplay();

                final Dialog dialog = new Dialog(this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setContentView(R.layout.default_popup);
                dialog.getWindow().setLayout(display.getWidth() * 90 / 100, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.setCancelable(false);

                TextView title = dialog.findViewById(R.id.title);
                TextView yes = dialog.findViewById(R.id.yes);
                TextView no = dialog.findViewById(R.id.no);
                yes.setText(getString(R.string.yes));
                title.setText(R.string.really_logout);
                no.setVisibility(View.VISIBLE);

                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        removeToken();
                        editor.clear();
                        editor.commit();
                        dialog.dismiss();
                        GetSet.logout();
                        ProfileActivity.this.finish();
                        Intent j = new Intent(ProfileActivity.this, WelcomeActivity.class);
                        startActivity(j);
                    }
                });

                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                if (!dialog.isShowing()) {
                    dialog.show();
                }
                break;
            case R.id.phNoVerified:
            case R.id.profPhoneno:
                ApplicationClass.hideSoftKeyboard(ProfileActivity.this, view);
                verifyMobileNo(view);
                break;
            case R.id.profName:
                display = this.getWindowManager().getDefaultDisplay();
                final NameDialogBinding namebinding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.name_dialog, null, false);
                forgotdialog = new BottomSheetDialog(this);
                forgotdialog.setContentView(namebinding.getRoot());
                forgotdialog.setCanceledOnTouchOutside(false);
                forgotdialog.setCancelable(false);
                forgotdialog.show();

                namebinding.emaillay.setHint(getResources().getString(R.string.name));
                namebinding.email.setFocusable(true);
                namebinding.cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ApplicationClass.hideSoftKeyboard(ProfileActivity.this, view);
                        forgotdialog.dismiss();
                    }
                });
                namebinding.saveBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (namebinding.email.getText().toString().equals("")) {
                            namebinding.emaillay.setError(getString(R.string.name_error));
                            ApplicationClass.requestFocus(ProfileActivity.this, v, true);
                        } else {
                            ApplicationClass.hideSoftKeyboard(ProfileActivity.this, v);
                            progressDialog.show();
                            forgotdialog.dismiss();
                            changeName(namebinding.email.getText().toString().trim());
                        }
                    }
                });
                if (!forgotdialog.isShowing()) {
                    forgotdialog.show();
                }

                break;
        }
    }

    private void changeName(final String name) {
        HashMap<String, String> map = new HashMap<>();
        map.put(Constants.TAG_USER_ID, GetSet.getUserId());
        map.put(Constants.TAG_FULL_NAME, name);
        Call<Map<String, String>> call3 = apiInterface.updateProfile(GetSet.getToken(), map);
        call3.enqueue(new Callback<Map<String, String>>() {
            @Override
            public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                try {
                    Map<String, String> data = response.body();
                    if (data.get(Constants.TAG_STATUS).equals("true")) {
                        progressDialog.dismiss();
                        profBinding.profName.setText(name);
                        GetSet.setFullName(name);
                        editor.putString("fullName", name);
                        editor.commit();
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.update_name_successful), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.update_name_unsuccessful), Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<Map<String, String>> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private void changePhoneNumber(final String phoneNumber, final String countryCode) {
        HashMap<String, String> map = new HashMap<>();
        map.put(Constants.TAG_USER_ID, GetSet.getUserId());
        map.put(Constants.TAG_COUNTRY_CODE, countryCode);
        map.put(Constants.TAG_PHONE_NUMBER, phoneNumber);
        Call<Map<String, String>> call3 = apiInterface.updateProfile(GetSet.getToken(), map);
        call3.enqueue(new Callback<Map<String, String>>() {
            @Override
            public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                try {
                    Map<String, String> data = response.body();
                    if (data.get(Constants.TAG_STATUS).equals("true")) {
                        progressDialog.dismiss();
                        profBinding.profPhoneno.setText(countryCode + phoneNumber);
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.profiledetailssucess), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.something_wrong), Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<Map<String, String>> call, Throwable t) {
                call.cancel();
            }
        });
    }
}