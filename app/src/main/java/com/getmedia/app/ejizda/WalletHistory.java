package com.hitasoft.app.cabso;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.hitasoft.app.model.WalletHistoryData;
import com.hitasoft.app.utils.ApiClient;
import com.hitasoft.app.utils.ApiInterface;
import com.hitasoft.app.utils.Constants;
import com.hitasoft.app.utils.GetSet;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hitasoft on 22/4/18.
 */

public class WalletHistory extends AppCompatActivity implements View.OnClickListener {
    ImageView backBtn, nullImage;
    RecyclerView recyclerView;
    RelativeLayout nullLay, progress;
    TextView nullText;
    ApiInterface apiInterface;
    RecyclerViewAdapter recyclerViewAdapter;
    ArrayList<WalletHistoryData.Result> datas = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wallet_history);

        backBtn = findViewById(R.id.backBtn);
        recyclerView = findViewById(R.id.recyclerView);
        nullLay = findViewById(R.id.nullLay);
        nullText = findViewById(R.id.nullText);
        nullImage = findViewById(R.id.nullImage);
        progress = findViewById(R.id.progress);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        backBtn.setVisibility(View.VISIBLE);

        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.recyclerview_divider));
        recyclerView.addItemDecoration(dividerItemDecoration);

        recyclerViewAdapter = new RecyclerViewAdapter(this, datas);
        recyclerView.setAdapter(recyclerViewAdapter);

        backBtn.setOnClickListener(this);

        nullImage.setImageResource(R.drawable.no_wallet);
        nullText.setText(getString(R.string.no_wallet));
        progress.setVisibility(View.VISIBLE);

        getData();
    }

    private void getData() {
        Call<WalletHistoryData> call3 = apiInterface.walletHistory(GetSet.getToken(), GetSet.getUserId());
        call3.enqueue(new Callback<WalletHistoryData>() {
            @Override
            public void onResponse(Call<WalletHistoryData> call, Response<WalletHistoryData> response) {
                try {
                    progress.setVisibility(View.GONE);
                    WalletHistoryData data = response.body();
                    Log.v("response", "response=" + new Gson().toJson(response));
                    if (data.status.equals("true")) {
                        datas.addAll(data.result);
                        recyclerViewAdapter.notifyDataSetChanged();
                    } else if (data.status.equals("false")) {

                    }

                    if (datas.size() == 0) {
                        nullLay.setVisibility(View.VISIBLE);
                    } else {
                        nullLay.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<WalletHistoryData> call, Throwable t) {
                call.cancel();
                progress.setVisibility(View.GONE);
                if (datas.size() == 0) {
                    nullLay.setVisibility(View.VISIBLE);
                } else {
                    nullLay.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                finish();
                break;
        }
    }

    public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

        ArrayList<WalletHistoryData.Result> Items;
        Context context;

        public RecyclerViewAdapter(Context context, ArrayList<WalletHistoryData.Result> Items) {
            this.Items = Items;
            this.context = context;
        }

        @Override
        public RecyclerViewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.history_item, parent, false);

            return new RecyclerViewAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final RecyclerViewAdapter.MyViewHolder holder, int position) {
            WalletHistoryData.Result data = Items.get(position);

            if (data.type.equals("credit")) {
                holder.image.setImageResource(R.drawable.up);
                holder.amount.setText(Constants.currency_symbol + " " + data.amount);
                holder.amount.setTextColor(ContextCompat.getColor(context, R.color.green));
            } else {
                holder.image.setImageResource(R.drawable.down);
                holder.amount.setText( Constants.currency_symbol + " " + data.amount);
                holder.amount.setTextColor(ContextCompat.getColor(context, R.color.red));
            }
            holder.transaction.setText(data.transaction);
            holder.date.setText(ApplicationClass.getDateTimeFromStamp(Long.parseLong(data.date)));
        }

        @Override
        public int getItemCount() {
            return Items.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView transaction, amount, date;
            ImageView image;

            public MyViewHolder(View view) {
                super(view);

                image = view.findViewById(R.id.image);
                transaction = view.findViewById(R.id.transaction);
                amount = view.findViewById(R.id.amount);
                date = view.findViewById(R.id.date);
            }
        }
    }
}
