package com.hitasoft.app.cabso;

import android.animation.ValueAnimator;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;

import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.hitasoft.app.helper.NetworkReceiver;
import com.hitasoft.app.utils.ApiClient;
import com.hitasoft.app.utils.ApiInterface;
import com.hitasoft.app.utils.Constants;
import com.hitasoft.app.utils.GetSet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.socket.client.Socket;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        NavigationView.OnNavigationItemSelectedListener, NetworkReceiver.ConnectivityReceiverListener,
        OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    public static ImageView userImage;
    DrawerLayout drawer;
    ImageView navBtn, mylocation, notifyBtn, appName;
    Toolbar toolbar;
    NavigationView navigationView;
    LinearLayout usrLayout;
    RelativeLayout contentLay;
    private List<LatLng> polyLineList;
    NetworkReceiver receiver;
    TextView userName, walletAmount;
    int exit = 0;
    Snackbar snackbar;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    EditText pickuplocation;
    String pointerLoc;
    Location mLastLocation;
    ApiInterface apiInterface;
    Animation slideup, slidedown;
    FrameLayout rootFrame;
    private Socket mSocket;
    private LatLng pointer;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        navBtn = findViewById(R.id.navBtn);
        notifyBtn = findViewById(R.id.notifyBtn);
        mylocation = findViewById(R.id.mylocation);
        drawer = findViewById(R.id.drawer_layout);
        receiver = new NetworkReceiver();
        polyLineList = new ArrayList<>();
        ApplicationClass.getInstance().setConnectivityListener(this);
        pickuplocation = findViewById(R.id.pickuplocation);
        rootFrame = findViewById(R.id.rootFrame);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        toolbar = findViewById(R.id.toolbar);
        navigationView = findViewById(R.id.nav_view);
        appName = findViewById(R.id.appName);
        contentLay = findViewById(R.id.contentLay);

        setSupportActionBar(toolbar);

        appName.setVisibility(View.VISIBLE);
        notifyBtn.setVisibility(View.VISIBLE);
        navBtn.setVisibility(View.VISIBLE);

        ApplicationClass app = (ApplicationClass) MainActivity.this.getApplication();
        mSocket = app.getSocket();
        mSocket.connect();
        // Boolean check = mSocket.connected();
        mSocket.emit("user", "Android");
        View header = navigationView.getHeaderView(0);

        userImage = header.findViewById(R.id.userImage);
        usrLayout = header.findViewById(R.id.usrLayout);
        userName = header.findViewById(R.id.userName);

        RelativeLayout walletLay = (RelativeLayout) MenuItemCompat.getActionView(navigationView.getMenu().
                findItem(R.id.wallet_menu));
        walletAmount = walletLay.findViewById(R.id.walletAmount);

        userName.setText(GetSet.getFullName());

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.temp);
        requestOptions.error(R.drawable.temp);
        requestOptions.circleCropTransform();
        Glide.with(MainActivity.this).load(GetSet.getImageUrl()).thumbnail(0.5f)
                .apply(requestOptions)
                .transition(new DrawableTransitionOptions().crossFade())
                .into(MainActivity.userImage);

        slideup = AnimationUtils.loadAnimation(this, R.anim.slide_up);
        slidedown = AnimationUtils.loadAnimation(this, R.anim.slide_down);

        navBtn.setOnClickListener(this);
        mylocation.setOnClickListener(this);
        navigationView.setNavigationItemSelectedListener(this);
        userImage.setOnClickListener(this);
        usrLayout.setOnClickListener(this);
        notifyBtn.setOnClickListener(this);
        pickuplocation.setOnClickListener(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer,
                null, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                userName.setText(GetSet.getFullName());
                Glide.with(MainActivity.this).load(GetSet.getImageUrl()).thumbnail(0.5f)
                        .apply(RequestOptions.circleCropTransform())
                        .transition(new DrawableTransitionOptions().crossFade())
                        .into(MainActivity.userImage);
                if (GetSet.getWallet() == null) {
                    walletAmount.setVisibility(View.GONE);
                } else {
                    walletAmount.setVisibility(View.VISIBLE);
                    walletAmount.setText(Constants.currency_symbol + GetSet.getWallet());
                }
                Log.v("Drawer", "Drawer Opened");
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                Log.v("Drawer", "Drawer Closed");
            }
        };
        drawer.addDrawerListener(toggle);
        drawer.post(new Runnable() {
            @Override
            public void run() {
                toggle.syncState();
            }
        });

        navigationView.post(new Runnable() {
            @Override
            public void run() {
                Resources r = getResources();
                DisplayMetrics metrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(metrics);
                int width = metrics.widthPixels;

                float screenWidth = width / r.getDisplayMetrics().density;
                float navWidth = screenWidth - 56;

                navWidth = Math.min(navWidth, 320);

                int newWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, navWidth, r.getDisplayMetrics());

                DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) navigationView.getLayoutParams();
                params.width = newWidth;
                navigationView.setLayoutParams(params);
            }
        });

        snackbar = Snackbar.make(findViewById(R.id.parentLay), getString(R.string.exit_msg), Snackbar.LENGTH_SHORT);
        snackbar.addCallback(new Snackbar.Callback() {

            @Override
            public void onDismissed(Snackbar snackbar, int event) {
                exit = 0;
            }

            @Override
            public void onShown(Snackbar snackbar) {
                exit = 1;
            }
        });
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Log.v("onNavigation", "=" + item.getTitle());
        int id = item.getItemId();
        switchActivityByNavigation(id, item);
        drawer.closeDrawer(Gravity.LEFT);
        return false;
    }

    private void switchActivityByNavigation(int id, MenuItem item) {
        switch (id) {
            case R.id.yourrides_menu:
                Intent ride = new Intent(MainActivity.this, RideActivity.class);
                startActivity(ride);
                break;
            case R.id.wallet_menu:
                Intent wallet = new Intent(MainActivity.this, WalletActivity.class);
                startActivity(wallet);
                break;
            case R.id.invite_menu:
                Intent invite = new Intent(MainActivity.this, InviteActivity.class);
                startActivity(invite);
                break;
            case R.id.help_menu:
                Intent helpIntent = new Intent(MainActivity.this, HelpActivity.class);
                startActivity(helpIntent);
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setRotateGesturesEnabled(false);
        // startRevealAnimation();
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.mapstyle));
            if (!success) {
                Log.e("error", "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("error", "Can't find style. Error: ", e);
        }
        //need to add Mpermision here
        if (ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
        }

    }




    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }


    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(17));

        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            CoordinatorLayout.LayoutParams params =
                    (CoordinatorLayout.LayoutParams) contentLay.getLayoutParams();
            params.setBehavior(null);
            contentLay.requestLayout();
        }

        mMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
            @Override
            public void onCameraMoveStarted(int reason) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    toolbar.setVisibility(View.GONE);
                    toolbar.startAnimation(slideup);
                }
                mylocation.setVisibility(View.GONE);
                mylocation.startAnimation(slidedown);
            }
        });
        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                try {
                    pointer = mMap.getCameraPosition().target;
                    Log.v("Center", "lattitude=" + pointer.latitude + " &longitude=" + pointer.longitude);
                    new GetLocationAsync(pointer.latitude, pointer.longitude).execute().get();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    toolbar.clearAnimation();
                    toolbar.setVisibility(View.VISIBLE);
                }
                mylocation.setVisibility(View.VISIBLE);
                mylocation.startAnimation(slideup);
            }
        });

    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    private void settoCurrentLocation() {
        FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        try {
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // GPS location can be null if GPS is switched off
                            if (location != null) {
                                LatLng latlng = new LatLng(location.getLatitude(),
                                        location.getLongitude());
                                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng, 17));
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d("MapDemoActivity", "Error trying to get last GPS location");
                            e.printStackTrace();
                        }
                    });
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.navBtn:
                drawer.openDrawer(Gravity.LEFT);
                break;
            case R.id.mylocation:
                settoCurrentLocation();
                break;
            case R.id.userImage:
            case R.id.usrLayout:

                Intent p = new Intent(this, ProfileActivity.class);
                p.putExtra("userId", GetSet.getUserId());
                startActivity(p);
                drawer.closeDrawer(Gravity.LEFT);

                break;
            case R.id.pickuplocation:
                Intent b = new Intent(MainActivity.this, SelectLocationActivity.class);
                b.putExtra("pickuppoint", pointerLoc);
                b.putExtra("pickuplat", pointer.latitude);
                b.putExtra("pickuplng", pointer.longitude);
                startActivity(b);
                break;
            case R.id.notifyBtn:
                Intent i = new Intent(MainActivity.this, NotificationActivity.class);
                startActivity(i);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (exit == 0) {
            snackbar.show();
        } else {
            MainActivity.this.finishAffinity();
        }
    }

 /*   void startRevealAnimation() {

        int cx = rootFrame.getMeasuredWidth() / 2;
        int cy = rootFrame.getMeasuredHeight() / 2;

        Animator anim = ViewAnimationUtils.createCircularReveal(drawer, cx, cy, 50, rootFrame.getWidth());

        anim.setDuration(500);
        anim.setInterpolator(new AccelerateInterpolator(2));
        anim.addListener(new AnimatorListenerAdapter() {

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);

                toolbar.setVisibility(View.VISIBLE);
                mylocation.setVisibility(View.VISIBLE);
            }
        });

        anim.start();
    }*/

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        Log.v("isConnected", "isConnected=" + isConnected);
        ApplicationClass.showSnack(this, findViewById(R.id.rootFrame), isConnected);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, ApplicationClass.intentFilter);
        if (NetworkReceiver.isConnected()) {
            ApplicationClass.showSnack(this, findViewById(R.id.rootFrame), true);
        } else {
            ApplicationClass.showSnack(this, findViewById(R.id.rootFrame), false);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
        ApplicationClass.showSnack(this, findViewById(R.id.rootFrame), true);
    }

    private class GetLocationAsync extends AsyncTask<String, Void, String> {

        double x, y;
        StringBuilder str;
        private Geocoder geocoder;
        private List<Address> addresses;

        public GetLocationAsync(double latitude, double longitude) {
            x = latitude;
            y = longitude;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                geocoder = new Geocoder(MainActivity.this, getResources().getConfiguration().locale);
                addresses = geocoder.getFromLocation(x, y, 1);
                if (geocoder.isPresent() && addresses.size() > 0) {
                    pointerLoc = addresses.get(0).getAddressLine(0);
                }
            } catch (IOException e) {
                Log.e("tag", e.getMessage());
            }
            return null;

        }
    }
}
