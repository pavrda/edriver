package com.hitasoft.app.cabso;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.hitasoft.app.external.FontCache;
import com.hitasoft.app.helper.NetworkReceiver;
import com.hitasoft.app.model.RideHistoryData;
import com.hitasoft.app.utils.ApiClient;
import com.hitasoft.app.utils.ApiInterface;
import com.hitasoft.app.utils.Constants;
import com.hitasoft.app.utils.GetSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hitasoft on 22/4/18.
 */

public class RideActivity extends AppCompatActivity implements NetworkReceiver.ConnectivityReceiverListener, View.OnClickListener {

    ImageView backBtn;
    TextView title;
    TabLayout tabLayout;
    ViewPager viewPager;
    NetworkReceiver receiver;


    public static void bookingRides(ApiInterface apiInterface, final BookingAdapter bookingAdapter, final ArrayList<RideHistoryData.result> datas,
                                    final RelativeLayout nullLay, final RelativeLayout progress, SwipeRefreshLayout mSwipeRefreshLayout) {
        HashMap<String, String> map = new HashMap<>();
        map.put("user_id", GetSet.getUserId());
        map.put("type", "booking");

        Log.v("bookingRides params:", "" + map);
        Call<RideHistoryData> call3 = apiInterface.rideHistory(GetSet.getToken(), map);
        call3.enqueue(new Callback<RideHistoryData>() {
            @Override
            public void onResponse(Call<RideHistoryData> call,
                                   Response<RideHistoryData> response) {
                try {
                    Log.v("response", "response=" + new Gson().toJson(response));
                    progress.setVisibility(View.GONE);
                    if (mSwipeRefreshLayout != null && mSwipeRefreshLayout.isRefreshing()) {
                        datas.clear();
                        mSwipeRefreshLayout.setRefreshing(false);  // This hides the spinner
                    }
                    RideHistoryData rideData = response.body();
                    if (rideData.status.equals("true")) {
                        datas.addAll(rideData.result);
                        bookingAdapter.notifyDataSetChanged();
                    } else if (rideData.status.equals("false")) {

                    }

                    if (datas.size() == 0) {
                        bookingAdapter.notifyDataSetChanged();
                        nullLay.setVisibility(View.VISIBLE);
                    } else {
                        nullLay.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RideHistoryData> call, Throwable t) {
                Log.v("Failed", "TEST");
                call.cancel();
                if (mSwipeRefreshLayout != null && mSwipeRefreshLayout.isRefreshing()) {
                    mSwipeRefreshLayout.setRefreshing(false);  // This hides the spinner
                }
                progress.setVisibility(View.GONE);
                if (datas.size() == 0) {
                    nullLay.setVisibility(View.VISIBLE);
                } else {
                    nullLay.setVisibility(View.GONE);
                }
            }
        });
    }

    public static void historyRides(ApiInterface apiInterface, final HistoryAdapter historyAdapter, final ArrayList<RideHistoryData.result> datas,
                                    final RelativeLayout nullLay, final RelativeLayout progress, SwipeRefreshLayout mSwipeRefreshLayout) {
        HashMap<String, String> map = new HashMap<>();
        map.put("user_id", GetSet.getUserId());
        map.put("type", "history");

        Log.v("bookingRides params:", "" + map);
        Call<RideHistoryData> call3 = apiInterface.rideHistory(GetSet.getToken(), map);
        call3.enqueue(new Callback<RideHistoryData>() {
            @Override
            public void onResponse(Call<RideHistoryData> call, Response<RideHistoryData> response) {
                try {
                    Log.v("response", "response=" + new Gson().toJson(response));
                    progress.setVisibility(View.GONE);
                    if (mSwipeRefreshLayout != null && mSwipeRefreshLayout.isRefreshing()) {
                        datas.clear();
                        mSwipeRefreshLayout.setRefreshing(false);  // This hides the spinner
                    }
                    RideHistoryData rideData = response.body();
                    if (rideData.status.equals("true")) {
                        datas.addAll(rideData.result);
                        historyAdapter.notifyDataSetChanged();
                    } else if (rideData.status.equals("false")) {

                    }

                    if (datas.size() == 0) {
                        nullLay.setVisibility(View.VISIBLE);
                    } else {
                        nullLay.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RideHistoryData> call, Throwable t) {
                Log.v("Failed", "TEST");
                call.cancel();
                if (mSwipeRefreshLayout != null && mSwipeRefreshLayout.isRefreshing()) {
                    mSwipeRefreshLayout.setRefreshing(false);  // This hides the spinner
                }
                progress.setVisibility(View.GONE);
                if (datas.size() == 0) {
                    nullLay.setVisibility(View.VISIBLE);
                } else {
                    nullLay.setVisibility(View.GONE);
                }
            }
        });
    }

    public static void RideDetail(Context context, ApiInterface apiInterface, String onride_id, String from) {
        HashMap<String, String> map = new HashMap<>();
        map.put("user_id", GetSet.getUserId());
        map.put("onride_id", onride_id);
        map.put("type", from);

        Log.v("RideDetail params:", "" + map);
        Call<Map<String, String>> call3 = apiInterface.ridedetails(GetSet.getToken(), map);
        call3.enqueue(new Callback<Map<String, String>>() {
            @Override
            public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                try {
                    Log.v("RideDetail", "response=" + new Gson());
                    Map<String, String> rideData = response.body();
                    if (rideData != null && !rideData.isEmpty()) {
                        if (rideData.get("status").equals("true")) {
                            Intent b = new Intent(context, MapsActivity.class);
                            b.putExtra("pickuppoint", rideData.get("pickup_location"));
                            b.putExtra("pickuplat", Double.valueOf(rideData.get("pickup_lat")));
                            b.putExtra("pickuplng", Double.valueOf(rideData.get("pickup_lng")));
                            b.putExtra("droppoint", rideData.get("drop_location"));
                            b.putExtra("baseprice", rideData.get("approx_price"));

                            b.putExtra("from", from);
                            b.putExtra("onrideid", onride_id);
                            b.putExtra("droplat", Double.valueOf(rideData.get("drop_lat")));
                            b.putExtra("droplng", Double.valueOf(rideData.get("drop_lng")));
                            context.startActivity(b);

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Map<String, String>> call, Throwable t) {
                Log.v("Failed", "TEST");
                call.cancel();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ride_activity);
        backBtn = findViewById(R.id.backBtn);
        title = findViewById(R.id.title);
        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewPager);
        receiver = new NetworkReceiver();
        ApplicationClass.getInstance().setConnectivityListener(this);
        backBtn.setVisibility(View.VISIBLE);
        title.setVisibility(View.VISIBLE);

        title.setText(getString(R.string.your_rides));

        backBtn.setOnClickListener(this);

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        changeTabsFont();
    }

    // For set custom font in tab
    private void changeTabsFont() {

        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(FontCache.get("font_regular.ttf", this));
                }
            }
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Booking(), getString(R.string.booking));
        adapter.addFragment(new History(), getString(R.string.history));
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backBtn:
                finish();
                break;
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        Log.v("isConnected", "isConnected=" + isConnected);
        ApplicationClass.showSnack(this, findViewById(R.id.parentLay), isConnected);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, ApplicationClass.intentFilter);
        if (NetworkReceiver.isConnected()) {
            ApplicationClass.showSnack(this, findViewById(R.id.parentLay), true);
        } else {
            ApplicationClass.showSnack(this, findViewById(R.id.parentLay), false);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
        ApplicationClass.showSnack(this, findViewById(R.id.parentLay), true);
    }

    public static class Booking extends Fragment {

        BookingAdapter bookingAdapter;
        RecyclerView recyclerView;
        LinearLayoutManager linearLayoutManager;
        SwipeRefreshLayout mSwipeRefreshLayout = null;
        ApiInterface apiInterface;
        RelativeLayout nullLay, progress;
        TextView nullText;
        ImageView nullImage;
        ArrayList<RideHistoryData.result> datas = new ArrayList<>();

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.ride_recyclerview_layout, container, false);

            recyclerView = rootView.findViewById(R.id.recyclerView);
            mSwipeRefreshLayout = rootView.findViewById(R.id.swipeRefreshLayout);
            nullLay = rootView.findViewById(R.id.nullLay);
            nullText = rootView.findViewById(R.id.nullText);
            nullImage = rootView.findViewById(R.id.nullImage);
            progress = rootView.findViewById(R.id.progress);

            nullImage.setImageResource(R.drawable.no_ride);
            nullText.setText(getString(R.string.no_ride));

            mSwipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
            return rootView;
        }

        @Override
        public void onResume() {
            super.onResume();
            mSwipeRefreshLayout.setRefreshing(true);
            bookingRides(apiInterface, bookingAdapter, datas, nullLay, progress, mSwipeRefreshLayout);
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            apiInterface = ApiClient.getClient().create(ApiInterface.class);

            linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));

            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    bookingRides(apiInterface, bookingAdapter, datas, nullLay, progress, mSwipeRefreshLayout);
                    Log.v("Onrefresh:", "On refresh");
                }
            });

            bookingAdapter = new BookingAdapter(apiInterface, getActivity(), datas);
            recyclerView.setAdapter(bookingAdapter);
            //    bookingRides(apiInterface, bookingAdapter, datas, nullLay, progress, mSwipeRefreshLayout);
        }
    }

    public static class BookingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private final int VIEW_TYPE_ITEM = 0;
        private final int VIEW_TYPE_LOADING = 1;
        ArrayList<RideHistoryData.result> Items;
        Context context;
        ApiInterface apiInterface;

        public BookingAdapter(ApiInterface apiInterface, Context context, ArrayList<RideHistoryData.result> Items) {
            this.Items = Items;
            this.context = context;
            this.apiInterface = apiInterface;

        }

        @Override
        public int getItemViewType(int position) {
            return Items.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == VIEW_TYPE_ITEM) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ride_history_item, parent, false);
                return new MyViewHolder(view);
            } else if (viewType == VIEW_TYPE_LOADING) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_loader, parent, false);
                return new LoadingViewHolder(view);
            }
            return null;
        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
            if (holder instanceof MyViewHolder) {
                MyViewHolder viewHolder = (MyViewHolder) holder;
                RideHistoryData.result data = Items.get(position);

                viewHolder.type.setVisibility(View.VISIBLE);
                viewHolder.type.setText(data.ride_status);
                viewHolder.vehicleDetail.setText(data.vehicle_no);
                Glide.with(context).load(data.category_image).thumbnail(0.5f)
                        .apply(RequestOptions.circleCropTransform())
                        .transition(new DrawableTransitionOptions().crossFade())
                        .into(viewHolder.cabImage);
                viewHolder.amount.setText(Constants.currency_symbol + data.total_price);
                if (data.pickup_time != null) {
                    viewHolder.date.setText(ApplicationClass.getDateTimeFromStamp(Long.parseLong(data.pickup_time)));
                }
            } else if (holder instanceof LoadingViewHolder) {

            }
        }

        @Override
        public int getItemCount() {
            return Items.size();
        }

        public class LoadingViewHolder extends RecyclerView.ViewHolder {

            public LoadingViewHolder(View view) {
                super(view);
            }
        }

        public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            RelativeLayout mainLay;
            TextView type, date, vehicleDetail, amount;
            ImageView cabImage;

            public MyViewHolder(View view) {
                super(view);

                mainLay = view.findViewById(R.id.mainLay);
                type = view.findViewById(R.id.type);
                vehicleDetail = view.findViewById(R.id.vehicleDetail);
                date = view.findViewById(R.id.date);
                amount = view.findViewById(R.id.amount);
                cabImage = view.findViewById(R.id.cabImage);
                mainLay.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.mainLay:
                        String status = Items.get(getAdapterPosition()).ride_status;
                        if (status.equals("accepted") || status.equals("onride") || status.equals("ontheway")) {
                            RideDetail(context, apiInterface, Items.get(getAdapterPosition()).onride_id, status);

                        } else {
                            Intent ride = new Intent(context, RideDetail.class);
                            ride.putExtra("onride_id", Items.get(getAdapterPosition()).onride_id);
                            ride.putExtra("from", Items.get(getAdapterPosition()).ride_status);
                            ride.putExtra("pickuptime", Items.get(getAdapterPosition()).pickup_time);
                            ride.putExtra("vehicleno", Items.get(getAdapterPosition()).vehicle_no);
                            context.startActivity(ride);
                        }
                        break;

                }
            }
        }
    }

    public static class History extends Fragment {

        HistoryAdapter historyAdapter;
        RecyclerView recyclerView;
        LinearLayoutManager linearLayoutManager;
        SwipeRefreshLayout mSwipeRefreshLayout = null;
        ApiInterface apiInterface;
        RelativeLayout nullLay, progress;
        TextView nullText;
        ImageView nullImage;
        ArrayList<RideHistoryData.result> datas = new ArrayList<>();

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.ride_recyclerview_layout, container, false);

            recyclerView = rootView.findViewById(R.id.recyclerView);
            mSwipeRefreshLayout = rootView.findViewById(R.id.swipeRefreshLayout);
            nullLay = rootView.findViewById(R.id.nullLay);
            nullText = rootView.findViewById(R.id.nullText);
            nullImage = rootView.findViewById(R.id.nullImage);
            progress = rootView.findViewById(R.id.progress);

            nullImage.setImageResource(R.drawable.no_ride);
            nullText.setText(getString(R.string.no_ride));

            mSwipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
            return rootView;
        }

        @Override
        public void onResume() {
            super.onResume();
            mSwipeRefreshLayout.setRefreshing(true);
            historyRides(apiInterface, historyAdapter, datas, nullLay, progress, mSwipeRefreshLayout);
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            apiInterface = ApiClient.getClient().create(ApiInterface.class);

            linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));

            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    historyRides(apiInterface, historyAdapter, datas, nullLay, progress, mSwipeRefreshLayout);
                    Log.v("Onrefresh:", "On refresh");
                }
            });

            historyAdapter = new HistoryAdapter(getActivity(), datas);
            recyclerView.setAdapter(historyAdapter);

        }
    }

    public static class HistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private final int VIEW_TYPE_ITEM = 0;
        private final int VIEW_TYPE_LOADING = 1;
        ArrayList<RideHistoryData.result> Items;
        Context context;

        public HistoryAdapter(Context context, ArrayList<RideHistoryData.result> Items) {
            this.Items = Items;
            this.context = context;
        }

        @Override
        public int getItemViewType(int position) {
            return Items.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == VIEW_TYPE_ITEM) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ride_history_item, parent, false);
                return new MyViewHolder(view);
            } else if (viewType == VIEW_TYPE_LOADING) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_loader, parent, false);
                return new LoadingViewHolder(view);
            }
            return null;
        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
            if (holder instanceof MyViewHolder) {
                MyViewHolder viewHolder = (MyViewHolder) holder;
                RideHistoryData.result data = Items.get(position);

                viewHolder.type.setVisibility(View.GONE);
                viewHolder.amount.setVisibility(View.VISIBLE);
                if (data.ride_status.equals("completed")) {
                    viewHolder.amount.setText(Constants.currency_symbol + data.total_price);
                    viewHolder.cancelimage.setVisibility(View.INVISIBLE);
                } else if (data.ride_status.equals("scheduleridenotaccepted")) {
                    viewHolder.amount.setText(context.getString(R.string.schedule));
                    viewHolder.amount.setTextColor(context.getResources().getColor(R.color.red));
                    viewHolder.cancelimage.setVisibility(View.INVISIBLE);
                } else {
                    viewHolder.amount.setText(context.getString(R.string.cancelled));
                    viewHolder.amount.setTextColor(context.getResources().getColor(R.color.red));
                    viewHolder.cancelimage.setVisibility(View.VISIBLE);
                }
                viewHolder.vehicleDetail.setText(data.vehicle_no);
                Glide.with(context).load(data.category_image).thumbnail(0.5f)
                        .apply(RequestOptions.circleCropTransform())
                        .transition(new DrawableTransitionOptions().crossFade())
                        .into(viewHolder.cabImage);
                if (data.pickup_time != null) {
                    viewHolder.date.setText(ApplicationClass.getDateTimeFromStamp(Long.parseLong(data.pickup_time)));
                }
            } else if (holder instanceof LoadingViewHolder) {

            }
        }

        @Override
        public int getItemCount() {
            return Items.size();
        }

        public class LoadingViewHolder extends RecyclerView.ViewHolder {

            public LoadingViewHolder(View view) {
                super(view);
            }
        }

        public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            TextView type, date, vehicleDetail, amount;
            ImageView cabImage, cancelimage;
            RelativeLayout mainLay;

            public MyViewHolder(View view) {
                super(view);

                type = view.findViewById(R.id.type);
                mainLay = view.findViewById(R.id.mainLay);
                vehicleDetail = view.findViewById(R.id.vehicleDetail);
                date = view.findViewById(R.id.date);
                amount = view.findViewById(R.id.amount);
                cabImage = view.findViewById(R.id.cabImage);
                cancelimage = view.findViewById(R.id.cancelimage);

                mainLay.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.mainLay:
                        Intent ride = new Intent(context, RideDetail.class);
                        ride.putExtra("onride_id", Items.get(getAdapterPosition()).onride_id);
                        ride.putExtra("from", Items.get(getAdapterPosition()).ride_status);
                        ride.putExtra("pickuptime", Items.get(getAdapterPosition()).pickup_time);
                        ride.putExtra("vehicleno", Items.get(getAdapterPosition()).vehicle_no);
                        context.startActivity(ride);
                        break;
                }
            }
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
