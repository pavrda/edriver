package com.hitasoft.app.cabso;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.google.android.gms.wallet.Cart;
import com.google.android.gms.wallet.LineItem;
import com.google.gson.Gson;
import com.hitasoft.app.helper.NetworkReceiver;
import com.hitasoft.app.utils.ApiClient;
import com.hitasoft.app.utils.ApiInterface;
import com.hitasoft.app.utils.Constants;
import com.hitasoft.app.utils.GetSet;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hitasoft on 22/4/18.
 */

public class RideComplete extends AppCompatActivity implements NetworkReceiver.ConnectivityReceiverListener, View.OnClickListener {
    private static final int DROP_IN_REQUEST = 100;
    public static SharedPreferences pref;
    public static SharedPreferences.Editor editor;
    ImageView cardselect, cashselect;
    TextView totalWallet, rideFare, tax, wallet, totalBill, pay;
    AppCompatCheckBox checkBox;
    RelativeLayout cardlay, cashlay;
    String payment_type, onride_id, grandTotal = "", walletAmount = "";
    Display display;
    LinearLayout paymentLay;
    ApiInterface apiInterface;
    Map<String, String> rideData;
    ProgressDialog dialog;
    NetworkReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ride_complete);

        cardlay = findViewById(R.id.cardlay);
        cashlay = findViewById(R.id.cashlay);
        cardselect = findViewById(R.id.cardselect);
        paymentLay = findViewById(R.id.paymentLay);
        cashselect = findViewById(R.id.cashselect);
        totalWallet = findViewById(R.id.totalWallet);
        rideFare = findViewById(R.id.rideFare);
        tax = findViewById(R.id.tax);
        wallet = findViewById(R.id.wallet);
        totalBill = findViewById(R.id.totalBill);
        pay = findViewById(R.id.pay);
        checkBox = findViewById(R.id.checkBox);
        receiver = new NetworkReceiver();
        ApplicationClass.getInstance().setConnectivityListener(this);
        display = this.getWindowManager().getDefaultDisplay();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        onride_id = getIntent().getStringExtra("onride_id");
        pref = getApplicationContext().getSharedPreferences("SavedPref", MODE_PRIVATE);
        editor = pref.edit();
        completeRideDetail();

        cardlay.setOnClickListener(this);
        cashlay.setOnClickListener(this);
        pay.setOnClickListener(this);

        dialog = new ProgressDialog(RideComplete.this);
        dialog.setMessage(getString(R.string.pleasewait));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                setPaymentData();
            }
        });
    }

    public void completeRideDetail() {
        HashMap<String, String> map = new HashMap<>();
        map.put("user_id", GetSet.getUserId());
        map.put("onride_id", onride_id);

        Log.v("completeRide params:", "" + map);
        Call<Map<String, String>> call3 = apiInterface.completeridedetails(GetSet.getToken(), map);
        call3.enqueue(new Callback<Map<String, String>>() {
            @Override
            public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                try {
                    Log.v("response", "response=" + new Gson());
                    rideData = response.body();
                    if (rideData != null && !rideData.isEmpty()) {
                        if (rideData.get("status").equals("true")) {
                            setPaymentData();
                        } else if (rideData.get("status").equals("false")) {

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Map<String, String>> call, Throwable t) {
                Log.v("Failed", "TEST");
                call.cancel();
            }
        });
    }

    private void setPaymentData() {
        if (rideData != null && !rideData.isEmpty()) {
            rideFare.setText(Constants.currency_symbol + rideData.get("ride_fare"));
            tax.setText(Constants.currency_symbol + rideData.get("ride_tax"));
            totalWallet.setText(Constants.currency_symbol + rideData.get("walletmoney"));
            float total = Float.parseFloat(rideData.get("ride_total"));
            float walletamt = Float.parseFloat(rideData.get("walletmoney"));
            if (walletamt < 0.0) {
                checkBox.setEnabled(false);
            } else {
                checkBox.setEnabled(true);
            }
            float grantTotal;
            if (checkBox.isChecked()) {
                float tempTot = total - walletamt;
                if (tempTot < 0) {
                    grantTotal = 0;
                    wallet.setText("-" + Constants.currency_symbol + total);
                    this.walletAmount = String.valueOf(total);
                    paymentLay.setVisibility(View.INVISIBLE);
                } else {
                    grantTotal = tempTot;
                    wallet.setText("-" + Constants.currency_symbol + walletamt);
                    this.walletAmount = String.valueOf(walletamt);
                    paymentLay.setVisibility(View.VISIBLE);
                }
            } else {
                paymentLay.setVisibility(View.VISIBLE);
                grantTotal = total;
                wallet.setText("-" + Constants.currency_symbol + "0");
            }
            this.grandTotal = String.valueOf(grantTotal);
            totalBill.setText(Constants.currency_symbol + grantTotal);
        }
    }

    public void paymentCompleteDialog() {
        final Dialog dialog = new Dialog(RideComplete.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.payment_complete);
        dialog.getWindow().setLayout(display.getWidth(), display.getHeight());
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        TextView rideFare = dialog.findViewById(R.id.rideFare);
        TextView tax = dialog.findViewById(R.id.tax);
        TextView wallet = dialog.findViewById(R.id.wallet);
        TextView totalBill = dialog.findViewById(R.id.totalBill);

        rideFare.setText(Constants.currency_symbol + rideData.get("ride_fare"));
        tax.setText(Constants.currency_symbol + rideData.get("ride_tax"));
        if (checkBox.isChecked()) {
            wallet.setText(Constants.currency_symbol + walletAmount);
        } else {
            wallet.setText(Constants.currency_symbol + "0");
        }
        totalBill.setText(Constants.currency_symbol + grandTotal);

        if (!dialog.isShowing()) {
            dialog.show();
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
                RideComplete.this.finish();
                Intent ride = new Intent(RideComplete.this, WriteReview.class);
                ride.putExtra("onrideid", onride_id);
                startActivity(ride);
            }
        }, 4000);
    }

    private void paybyCredit(String clientToken) {
        HashMap<String, String> map = new HashMap<>();
        map.put("user_id", GetSet.getUserId());
        map.put("amount", grandTotal);
        map.put("onride_id", onride_id);
        map.put("basefare", rideData.get("basefare"));
        map.put("commissionamount", rideData.get("commissionamount"));
        map.put("driver_id", rideData.get("driver_id"));
        map.put("tax", rideData.get("ride_tax"));
        if (checkBox.isChecked()) {
            map.put("iswallet", "1");
            map.put("walletamount", walletAmount);
        } else {
            map.put("iswallet", "0");
        }
        map.put("paynonce", clientToken);

        Log.v("map", "map=" + map);
        Call<Map<String, String>> call3 = apiInterface.paybycredit(GetSet.getToken(), map);
        call3.enqueue(new Callback<Map<String, String>>() {
            @Override
            public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    Map<String, String> map = response.body();
                    Log.v("response", "response=" + map);
                    if (map.get("status").equals("true")) {
                        if (checkBox.isChecked()) {
                            editor.putString("wallet", map.get("walletmoney"));
                            editor.commit();
                            GetSet.setWallet(map.get("walletmoney"));
                        }
                        paymentCompleteDialog();
                    } else {
                        Toast.makeText(RideComplete.this, map.get("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<Map<String, String>> call, Throwable t) {
                call.cancel();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void paybyCash() {
        HashMap<String, String> map = new HashMap<>();
        map.put("user_id", GetSet.getUserId());
        map.put("amount", grandTotal);
        map.put("onride_id", onride_id);
        map.put("basefare", rideData.get("basefare"));
        map.put("driver_id", rideData.get("driver_id"));
        map.put("commissionamount", rideData.get("commissionamount"));
        map.put("tax", rideData.get("ride_tax"));
        if (checkBox.isChecked()) {
            map.put("iswallet", "1");
            map.put("walletamount", walletAmount);
        } else {
            map.put("iswallet", "0");
        }

        Log.v("map", "map=" + map);
        Call<Map<String, String>> call3 = apiInterface.paybycash(GetSet.getToken(), map);
        call3.enqueue(new Callback<Map<String, String>>() {
            @Override
            public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                try {
                    if (dialog != null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    Log.v("response", "response=" + new Gson().toJson(response));
                    Map<String, String> map = response.body();
                    if (map.get("status").equals("true")) {
                        if (checkBox.isChecked()) {
                            editor.putString("wallet", map.get("walletmoney"));
                            editor.commit();
                            GetSet.setWallet(map.get("walletmoney"));
                        }
                        paymentCompleteDialog();
                    } else {
                        Toast.makeText(RideComplete.this, map.get("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<Map<String, String>> call, Throwable t) {
                call.cancel();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void getClientToken(String from) {
        HashMap<String, String> map = new HashMap<>();
        map.put("user_id", GetSet.getUserId());

        Call<Map<String, String>> call3 = apiInterface.getclienttoken(GetSet.getToken(), map);
        call3.enqueue(new Callback<Map<String, String>>() {
            @Override
            public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                try {
                    Log.v("response", "response=" + new Gson().toJson(response));
                    final Map<String, String> map = response.body();
                    if (map.get("status").equals("true")) {

                        DropInResult.fetchDropInResult(RideComplete.this, map.get("clientToken"), new DropInResult.DropInResultListener() {
                            @Override
                            public void onError(Exception exception) {
                                // an error occurred
                                Log.v("TEST====", "onError");
                                Toast.makeText(RideComplete.this, exception.getMessage(),
                                        Toast.LENGTH_SHORT).show();
                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                            }

                            @Override
                            public void onResult(DropInResult result) {
                                Cart cart = Cart.newBuilder()
                                        .setCurrencyCode("USD")
                                        .setTotalPrice(grandTotal)
                                        .addLineItem(LineItem.newBuilder()
                                                .setCurrencyCode("USD")
                                                .setDescription("Description")
                                                .setQuantity("1")
                                                .setUnitPrice(grandTotal)
                                                .setTotalPrice(grandTotal)
                                                .build())
                                        .build();

                                DropInRequest dropInRequest = new DropInRequest()
                                        .tokenizationKey(map.get("clientToken"))
                                        .amount(grandTotal)
                                        .androidPayCart(cart);
                                startActivityForResult(dropInRequest.getIntent(RideComplete.this), DROP_IN_REQUEST);

                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                            }
                        });

                    } else {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        Toast.makeText(RideComplete.this, map.get("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<Map<String, String>> call, Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                Toast.makeText(RideComplete.this, getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();
                call.cancel();
            }
        });
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        Log.v("isConnected", "isConnected=" + isConnected);
        ApplicationClass.showSnack(this, findViewById(R.id.activity_complete), isConnected);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, ApplicationClass.intentFilter);
        if (NetworkReceiver.isConnected()) {
            ApplicationClass.showSnack(this, findViewById(R.id.activity_complete), true);
        } else {
            ApplicationClass.showSnack(this, findViewById(R.id.activity_complete), false);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
        ApplicationClass.showSnack(this, findViewById(R.id.activity_complete), true);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == DROP_IN_REQUEST) {
            if (resultCode == RESULT_OK) {
                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                String paymentMethodNonce = result.getPaymentMethodNonce().getNonce();
                Log.v("paymentMethodNonce", "paymentMethodNonce=" + paymentMethodNonce);
                // send paymentMethodNonce to your server
                dialog.show();
                paybyCredit(paymentMethodNonce);
            } else if (resultCode != RESULT_CANCELED) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
                Toast.makeText(RideComplete.this, ((Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR)).getMessage(),
                        Toast.LENGTH_SHORT).show();
            } else {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
                Toast.makeText(RideComplete.this, getString(R.string.payment_cancelled),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                break;
            case R.id.cardlay:
                payment_type = "card";
                cardselect.setVisibility(View.VISIBLE);
                cashselect.setVisibility(View.GONE);
                break;
            case R.id.cashlay:
                payment_type = "cash";
                cashselect.setVisibility(View.VISIBLE);
                cardselect.setVisibility(View.GONE);
                break;
            case R.id.pay:
                if (paymentLay.getVisibility() == View.INVISIBLE)
                    paybyCash();
                else if (cashselect.getVisibility() == View.VISIBLE) {
                    paybyCash();
                } else if (cardselect.getVisibility() == View.VISIBLE) {
                    dialog.show();
                    getClientToken("card");
                } else {
                    Toast.makeText(this, getString(R.string.payment_error), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
