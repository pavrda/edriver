package com.hitasoft.app.cabso;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.accountkit.AccessToken;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.hitasoft.app.cabso.databinding.ActivityLoginBinding;
import com.hitasoft.app.helper.NetworkReceiver;
import com.hitasoft.app.helper.SharedPrefManager;
import com.hitasoft.app.model.UserData;
import com.hitasoft.app.utils.ApiClient;
import com.hitasoft.app.utils.ApiInterface;
import com.hitasoft.app.utils.Constants;
import com.hitasoft.app.utils.GetSet;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, NetworkReceiver.ConnectivityReceiverListener {
    private static final int RC_SIGN_IN = 9001;
    private static final int APP_REQUEST_CODE = 9002;
    public static SharedPreferences pref;
    public static SharedPreferences.Editor editor;
    Dialog forgotdialog;
    ProgressDialog progressDialog;
    GoogleSignInClient mGoogleSignInClient;
    CallbackManager callbackManager;
    ApiInterface apiInterface;
    Snackbar snackbar;
    ActivityLoginBinding binding;
    NetworkReceiver receiver;
    int exit = 0;
    ImageView backBtn;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+", phoneNumber = "", countryCode = "";
    UserData userdata;

    private void addDeviceId(final Context context) {

        final String token = SharedPrefManager.getInstance(context).getDeviceToken();
        final String deviceId = android.provider.Settings.Secure.getString(context.getContentResolver(),
                android.provider.Settings.Secure.ANDROID_ID);

        Map<String, String> map = new HashMap<>();
        map.put("user_id", GetSet.getUserId());
        map.put("device_token", token);
        map.put("device_type", "1");
        map.put("device_id", deviceId);

        Log.v("addDeviceId:", "Params- " + map);
        Call<Map<String, String>> call3 = apiInterface.pushsignin(GetSet.getToken(), map);
        call3.enqueue(new Callback<Map<String, String>>() {
            @Override
            public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                Map<String, String> data = response.body();
                Log.v("addDeviceId:", "response- " + data);
                Intent b = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(b);
            }

            @Override
            public void onFailure(Call<Map<String, String>> call, Throwable t) {
                call.cancel();
            }
        });

        progressDialog.dismiss();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        receiver = new NetworkReceiver();
        ApplicationClass.getInstance().setConnectivityListener(this);
        pref = getApplicationContext().getSharedPreferences("SavedPref", MODE_PRIVATE);
        editor = pref.edit();

        backBtn = (ImageView) findViewById(R.id.backBtn);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.pleasewait));
        progressDialog.setCancelable(false);
        backBtn.setVisibility(View.VISIBLE);

        backBtn.setOnClickListener(this);
        binding.singnin.setOnClickListener(this);
        binding.createacc.setOnClickListener(this);
        binding.fbLay.setOnClickListener(this);
        binding.gplusLay.setOnClickListener(this);
        binding.forgotpassword.setOnClickListener(this);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {
    }


    public void initFacebook() {

        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject profile, GraphResponse response) {
                                        // Application code
                                        try {
                                            if (profile.has("email")) {
                                                binding.email.setText(profile.getString("email"));
                                                binding.password.setText("");
                                            } else {
                                                ApplicationClass.showToast(LoginActivity.this, "Please check your Facebook permissions", Toast.LENGTH_SHORT);
                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,first_name,last_name");
                        request.setParameters(parameters);
                        request.executeAsync();

                    }

                    @Override
                    public void onCancel() {
                        ApplicationClass.showToast(LoginActivity.this, "Facebook - Cancelled", Toast.LENGTH_SHORT);

                    }

                    @Override
                    public void onError(FacebookException exception) {

                        ApplicationClass.showToast(LoginActivity.this, "Facebook - " + exception.getMessage(), Toast.LENGTH_SHORT);
                        if (exception instanceof FacebookAuthorizationException) {
                            if (com.facebook.AccessToken.getCurrentAccessToken() != null) {
                                LoginManager.getInstance().logOut();
                            }
                        }
                    }
                });

    }

    @SuppressLint("RestrictedApi")
    public void initGplus() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        Log.d("handleSignInResult", "handleSignInResult:" + completedTask.isSuccessful());

        try {
            GoogleSignInAccount acct = completedTask.getResult(ApiException.class);

            binding.email.setText(acct.getEmail());
            binding.password.setText("");
        } catch (ApiException e) {
            Log.w("signInResult", "signInResult:failed code=" + e.getStatusCode());
        }

    }

    public void verifyMobileNo() {
        final Intent intent = new Intent(LoginActivity.this, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        LoginType.PHONE,
                        AccountKitActivity.ResponseType.TOKEN);
        configurationBuilder.setReadPhoneStateEnabled(true);
        configurationBuilder.setReceiveSMS(true);
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        startActivityForResult(intent, APP_REQUEST_CODE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, ApplicationClass.intentFilter);
        if (NetworkReceiver.isConnected()) {
            ApplicationClass.showSnack(this, findViewById(R.id.activity_login), true);
        } else {
            ApplicationClass.showSnack(this, findViewById(R.id.activity_login), false);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
        ApplicationClass.showSnack(this, findViewById(R.id.activity_login), true);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.v("onConnectionFailed", "onConnectionFailed");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.v("GoogleSignInApi", "GoogleSignInApi" + data);
        if (requestCode == RC_SIGN_IN) {
            Log.v("GoogleSignInApi", "GoogleSignInApi");
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
            if (progressDialog.isShowing())
                progressDialog.dismiss();
        } else if (requestCode == APP_REQUEST_CODE) {
            AccessToken accessToken = AccountKit.getCurrentAccessToken();
            if (accessToken != null) {
                //Handle Returning User
                AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
                    @Override
                    public void onSuccess(Account account) {
                        PhoneNumber phNumber = account.getPhoneNumber();
                        if (phNumber != null) {

                            phoneNumber = phNumber.getPhoneNumber();
                            countryCode = phNumber.getCountryCode();
                            changePhoneNumber(phoneNumber, countryCode);

                        }
                    }

                    @Override
                    public void onError(AccountKitError accountKitError) {
                        if (progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
            }

        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        Log.v("isConnected", "isConnected=" + isConnected);
        ApplicationClass.showSnack(this, findViewById(R.id.activity_login), isConnected);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                onBackPressed();
                break;
            case R.id.createacc:
                Intent b = new Intent(LoginActivity.this, SignupActivity.class);
                startActivity(b);

                break;
            case R.id.singnin:
                binding.passwordlay.setError(null);
                binding.emaillay.setError(null);
                if (binding.email.getText().toString().equals("")) {
                    binding.emaillay.setError(getString(R.string.emailempty_error));
                    requestFocus(binding.email);
                } else if (!binding.email.getText().toString().matches(emailPattern)) {
                    binding.emaillay.setError(getString(R.string.email_error));
                    requestFocus(binding.email);
                } else if (binding.password.getText().toString().equals("")) {
                    binding.passwordlay.setError(getString(R.string.passwordempty_error));
                    requestFocus(binding.password);
                } else {
                    ApplicationClass.hideSoftKeyboard(LoginActivity.this, view);
                    signin();

                }
                break;
            case R.id.fbLay:
                initFacebook();
                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("public_profile", "email"));
                break;
            case R.id.gplusLay:
                initGplus();
                @SuppressLint("RestrictedApi")
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                LoginActivity.this.startActivityForResult(signInIntent, RC_SIGN_IN);
                break;
            case R.id.forgotpassword:

                final ImageView dbackBtn;
                forgotdialog = new Dialog(this, R.style.DialogStyle);
                forgotdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                forgotdialog.setContentView(R.layout.forgot_dialog);

                dbackBtn = forgotdialog.findViewById(R.id.backBtn);
                dbackBtn.setVisibility(View.VISIBLE);
                forgotdialog.setCancelable(true);

                TextView reset = forgotdialog.findViewById(R.id.reset);
                final EditText email = forgotdialog.findViewById(R.id.email);
                final TextInputLayout emaillay = forgotdialog.findViewById(R.id.emaillay);

                emaillay.setHint(getResources().getString(R.string.email));
                email.setFocusable(true);
                dbackBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        forgotdialog.dismiss();
                    }
                });
                reset.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (email.getText().toString().equals("")) {
                            emaillay.setError(getString(R.string.emailempty_error));
                            requestFocus(email);
                        } else if (!email.getText().toString().matches(emailPattern)) {
                            emaillay.setError(getString(R.string.email_error));
                            requestFocus(email);
                        } else {
                            ApplicationClass.hideSoftKeyboard(LoginActivity.this, v);
                            progressDialog.show();
                            forgotPassword(email.getText().toString().trim());
                        }
                    }
                });
                if (!forgotdialog.isShowing()) {
                    forgotdialog.show();
                }
                break;
        }
    }

    private void forgotPassword(String email) {
        progressDialog.show();
        Map<String, String> map = new HashMap<>();
        map.put("email", email);
        Call<HashMap<String, String>> call3 = apiInterface.forgotPassword(map);
        call3.enqueue(new Callback<HashMap<String, String>>() {
            @Override
            public void onResponse(Call<HashMap<String, String>> call, Response<HashMap<String, String>> response) {
                HashMap<String, String> data = response.body();
                Log.v("forgotPassword", "Response=" + data);

                if (data.get(Constants.TAG_STATUS).equals("true")) {
                    Toast.makeText(LoginActivity.this, data.get(Constants.TAG_MESSSAGE), Toast.LENGTH_SHORT).show();
                    if (forgotdialog.isShowing()) {
                        forgotdialog.dismiss();
                    }
                } else
                    Toast.makeText(LoginActivity.this, data.get(Constants.TAG_MESSSAGE), Toast.LENGTH_SHORT).show();
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<HashMap<String, String>> call, Throwable t) {
                call.cancel();
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void changePhoneNumber(final String phoneNumber, final String countryCode) {
        progressDialog.show();
        HashMap<String, String> map = new HashMap<>();
        map.put(Constants.TAG_USER_ID, userdata.user_id);
        map.put(Constants.TAG_COUNTRY_CODE, countryCode);
        map.put(Constants.TAG_PHONE_NUMBER, phoneNumber);
        Call<Map<String, String>> call3 = apiInterface.updateProfile(userdata.token, map);
        call3.enqueue(new Callback<Map<String, String>>() {
            @Override
            public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                try {
                    Map<String, String> data = response.body();
                    if (data.get(Constants.TAG_STATUS).equals("true")) {

                        GetSet.Logingin(userdata.user_id, userdata.full_name, userdata.imageurl, userdata.token, userdata.walletmoney);

                        editor.putBoolean("isLogged", true);
                        editor.putString("userId", userdata.user_id);
                        editor.putString("userImage", userdata.imageurl);
                        editor.putString("fullName", userdata.full_name);
                        editor.putString("token", userdata.token);
                        editor.putString("password", userdata.password);
                        editor.putString("wallet", userdata.walletmoney);
                        editor.commit();
                        GetSet.setPassword(userdata.password);
                        GetSet.setToken(userdata.token);
                        GetSet.setWallet(userdata.walletmoney);
                        addDeviceId(LoginActivity.this);
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.something_wrong), Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (progressDialog.isShowing())
                    progressDialog.dismiss();

            }

            @Override
            public void onFailure(Call<Map<String, String>> call, Throwable t) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                call.cancel();
            }
        });
    }

    public void signin() {
        progressDialog.show();
        HashMap<String, String> map = new HashMap<>();
        map.put(Constants.TAG_EMAIL, binding.email.getText().toString());
        map.put(Constants.TAG_PASSWORD, binding.password.getText().toString());

        Log.v("Login params:", "" + map);
        Call<UserData> call3 = apiInterface.signin(map);
        call3.enqueue(new Callback<UserData>() {
            @Override
            public void onResponse(Call<UserData> call, Response<UserData> response) {
                try {
                    Log.v("response", "response=" + new Gson().toJson(response));
                    userdata = response.body();

                    if (userdata.status.equals("true")) {
                        verifyMobileNo();

                    } else if (userdata.status.equals("false")) {
                        binding.password.setText("");
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), userdata.message, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<UserData> call, Throwable t) {
                Log.v("Login Failed", "TEST");
                call.cancel();
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }
}

