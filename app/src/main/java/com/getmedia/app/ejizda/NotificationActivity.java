package com.hitasoft.app.cabso;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.hitasoft.app.cabso.databinding.ActivityNotificationBinding;
import com.hitasoft.app.cabso.databinding.NotificationItemBinding;
import com.hitasoft.app.model.NotificationData;
import com.hitasoft.app.utils.ApiClient;
import com.hitasoft.app.utils.ApiInterface;
import com.hitasoft.app.utils.GetSet;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "NotificationActivity";
    public static NotificationData data = null;
    ActivityNotificationBinding binding;
    ImageView backBtn, nullImage;
    RecyclerAdapter recyclerAdapter;
    LinearLayoutManager linearLayoutManager;
    ArrayList<NotificationData.Result> notificationList = new ArrayList<>();
    ApiInterface apiInterface;
    RelativeLayout nullLay, progress;
    TextView nullText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_notification);
        backBtn = findViewById(R.id.backBtn);
        nullLay = findViewById(R.id.nullLay);
        nullText = findViewById(R.id.nullText);
        nullImage = findViewById(R.id.nullImage);
        progress = findViewById(R.id.progress);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        binding.recyclerView.setLayoutManager(linearLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.recyclerview_divider));
        binding.recyclerView.addItemDecoration(dividerItemDecoration);

        backBtn.setVisibility(View.VISIBLE);
        nullImage.setImageResource(R.drawable.no_notification);
        nullText.setText(getString(R.string.no_notification));

        recyclerAdapter = new RecyclerAdapter(this, notificationList);
        binding.recyclerView.setAdapter(recyclerAdapter);

        progress.setVisibility(View.VISIBLE);
        getUsrNotifications();

        backBtn.setOnClickListener(this);
    }

    private void getUsrNotifications() {
        Call<NotificationData> call3 = apiInterface.getNotifications(GetSet.getToken(), GetSet.getUserId());
        call3.enqueue(new Callback<NotificationData>() {
            @Override
            public void onResponse(Call<NotificationData> call, Response<NotificationData> response) {
                try {
                    Log.v("response", "response=" + new Gson().toJson(response));
                    progress.setVisibility(View.GONE);
                    data = response.body();
                    Log.v(TAG, "notificationData=" + data.result);
                    if (data.status.equals("true")) {
                        notificationList.addAll(data.result);
                        recyclerAdapter.notifyDataSetChanged();
                    } else if (data.status.equals("false")) {

                    }

                    if (notificationList.size() == 0) {
                        nullLay.setVisibility(View.VISIBLE);
                    } else {
                        nullLay.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<NotificationData> call, Throwable t) {
                call.cancel();
                progress.setVisibility(View.GONE);
                if (notificationList.size() == 0) {
                    nullLay.setVisibility(View.VISIBLE);
                } else {
                    nullLay.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backBtn:
                onBackPressed();
                break;
        }
    }

    private class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
        ArrayList<NotificationData.Result> notifyList;
        Context context;

        public RecyclerAdapter(Context context, ArrayList<NotificationData.Result> items) {
            notifyList = items;
            this.context = context;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            NotificationItemBinding itemBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.notification_item, parent, false);
            return new ViewHolder(itemBinding);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            holder.getBinding().notificationTitle.setText(notifyList.get(position).title);
            holder.getBinding().notificaitonText.setText(notifyList.get(position).message);
            holder.getBinding().notificaitonTime.setText(ApplicationClass.getDateTimeFromStamp(Long.parseLong(
                    notifyList.get(position).notified_at)));
            if (notifyList.get(position).title.contains("cancel") || notifyList.get(position).title.contains("decline")) {
                holder.getBinding().notificationImage.setImageDrawable(getResources().getDrawable(R.drawable.decline));
                holder.getBinding().notificationImage.setBackgroundColor(getResources().getColor(R.color.red));
            } else if (notifyList.get(position).title.equals("admin") || notifyList.get(position).title.equals("Admin")) {
                holder.getBinding().notificationImage.setImageDrawable(getResources().getDrawable(R.drawable.admin));
                holder.getBinding().notificationImage.setBackgroundColor(getResources().getColor(R.color.blue));
            } else {
                holder.getBinding().notificationImage.setImageDrawable(getResources().getDrawable(R.drawable.tick));
                holder.getBinding().notificationImage.setBackgroundColor(getResources().getColor(R.color.green));
            }
        }

        @Override
        public int getItemCount() {
            return notifyList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            NotificationItemBinding itemBinding;

            public ViewHolder(NotificationItemBinding notifyBinding) {
                super(notifyBinding.getRoot());
                itemBinding = notifyBinding;
            }

            public NotificationItemBinding getBinding() {
                return itemBinding;
            }
        }

    }
}
