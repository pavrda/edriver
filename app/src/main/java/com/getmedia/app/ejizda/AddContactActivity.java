package com.hitasoft.app.cabso;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.hitasoft.app.cabso.databinding.ActivityAddContactBinding;
import com.hitasoft.app.helper.NetworkReceiver;
import com.hitasoft.app.model.ProfileData;
import com.hitasoft.app.utils.ApiClient;
import com.hitasoft.app.utils.ApiInterface;
import com.hitasoft.app.utils.Constants;
import com.hitasoft.app.utils.GetSet;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hitasoft on 21/3/18.
 */

public class AddContactActivity extends AppCompatActivity implements View.OnClickListener, NetworkReceiver.ConnectivityReceiverListener {
    ActivityAddContactBinding binding;
    ImageView backBtn;
    ProgressDialog progressDialog;
    NetworkReceiver receiver;
    String TAG = "AddContact";
    int PICK_CONTACT = 100;
    ApiInterface apiInterface;
    int updatePosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_contact);
        backBtn = findViewById(R.id.backBtn);
        receiver = new NetworkReceiver();
        ApplicationClass.getInstance().setConnectivityListener(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.pleasewait));
        progressDialog.setCancelable(false);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        if (getIntent().getStringExtra("from").equals("update")) {
            updatePosition = getIntent().getIntExtra("updatedPosition", 0);
            binding.name.setText(getIntent().getStringExtra("oldName"));
            binding.phoneNumber.setText(getIntent().getStringExtra("oldPhno"));
        }
        backBtn.setVisibility(View.VISIBLE);

        backBtn.setOnClickListener(this);
        binding.save.setOnClickListener(this);
        binding.phoneContactlay.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_CONTACT) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    String cNumber = "";
                    Uri contactData = data.getData();
                    Cursor cursor = managedQuery(contactData, null, null, null, null);

                    while (cursor.moveToNext()) {
                        String id
                                = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                        String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        String hasPhone = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                        if (hasPhone.equalsIgnoreCase("1")) {
                            Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id, null, null);
                            phones.moveToFirst();
                            cNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        }
                        binding.phoneNumber.setText(cNumber);
                        binding.name.setText(name);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(AddContactActivity.this, "Unsupported Format", Toast.LENGTH_SHORT).show();

                }
            }
        }

    }

    private JSONArray listToJSONArray(ArrayList<ProfileData.EmergencyContact> list, String keyName) {
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < list.size(); i++) {
            ProfileData.EmergencyContact emergencyContact = list.get(i);
            HashMap hashMap = new HashMap();
            hashMap.put("name", emergencyContact.name);
            hashMap.put("phone_no", emergencyContact.phone_no);
            JSONObject jsonObject = new JSONObject(hashMap);
            jsonArray.put(jsonObject);
        }
        return jsonArray;
    }

    /*API Integration*/

    /**
     * Function for add an Emergency Contact
     */

    private void emergencyContact(JSONArray jsonObject) {
        Map<String, String> map = new HashMap<>();
        map.put(Constants.TAG_USER_ID, GetSet.getUserId());
        map.put(Constants.TAG_EMERGENCY_CONTACT, jsonObject.toString());

        Log.v(TAG, "emergencyContactParams:" + map);
        Call<Map<String, String>> call3 = apiInterface.updateProfile(GetSet.getToken(), map);
        call3.enqueue(new Callback<Map<String, String>>() {
            @Override
            public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                Map<String, String> data = response.body();
                if (data.get(Constants.TAG_STATUS).equals("true")) {
                    Log.v(TAG, "emergencyContactResponse=" + data.get(Constants.TAG_MESSSAGE));
                    Toast.makeText(AddContactActivity.this, data.get(Constants.TAG_MESSSAGE), Toast.LENGTH_SHORT).show();
                    finish();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<Map<String, String>> call, Throwable t) {
                progressDialog.dismiss();
                call.cancel();
            }
        });
    }

    /**
     * Function for Onclick Event
     */

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backBtn:
                onBackPressed();
                break;
            case R.id.save:
                binding.namelay.setError(null);
                binding.phoneNumberLay.setError(null);
                if (binding.name.getText().toString().equals("")) {
                    binding.namelay.setError(getString(R.string.fullname_error));
                    ApplicationClass.requestFocus(AddContactActivity.this, binding.name, true);
                } else if (binding.phoneNumber.getText().toString().equals("")) {
                    binding.namelay.setErrorEnabled(false);
                    binding.phoneNumberLay.setError(getString(R.string.phonenumber_error));
                    ApplicationClass.requestFocus(AddContactActivity.this, binding.phoneNumber, true);
                } else {
                    progressDialog.show();
                    binding.phoneNumberLay.setErrorEnabled(false);
                    ProfileData.EmergencyContact emergencyContact = new ProfileData.EmergencyContact();
                    emergencyContact.name = binding.name.getText().toString();
                    emergencyContact.phone_no = binding.phoneNumber.getText().toString();

                    Log.v(TAG, "from value=" + getIntent().getStringExtra("from"));

                    if (getIntent().getStringExtra("from").equals("update")) {
                        ProfileActivity.emergencyList.set(updatePosition, emergencyContact);
                    } else {
                        Log.v(TAG, "add");
                        ProfileActivity.emergencyList.add(emergencyContact);
                        Log.v(TAG, "Emergency List=" + ProfileActivity.emergencyList);
                    }

                    JSONArray jsonObject = listToJSONArray(ProfileActivity.emergencyList, "emergency_contact");
                    emergencyContact(jsonObject);
                    Log.v(TAG, "jsonObject=" + jsonObject);
                }
                break;
            case R.id.phoneContactlay:
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent, PICK_CONTACT);
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        Log.v("isConnected", "isConnected=" + isConnected);
        ApplicationClass.showSnack(this, findViewById(R.id.activity_add_contact), isConnected);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, ApplicationClass.intentFilter);
        if (NetworkReceiver.isConnected()) {
            ApplicationClass.showSnack(this, findViewById(R.id.activity_add_contact), true);
        } else {
            ApplicationClass.showSnack(this, findViewById(R.id.activity_add_contact), false);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
        ApplicationClass.showSnack(this, findViewById(R.id.activity_add_contact), true);
    }
}
