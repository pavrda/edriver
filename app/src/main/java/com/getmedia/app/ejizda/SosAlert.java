package com.hitasoft.app.cabso;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.hitasoft.app.model.ProfileData;
import com.hitasoft.app.utils.ApiClient;
import com.hitasoft.app.utils.ApiInterface;
import com.hitasoft.app.utils.Constants;
import com.hitasoft.app.utils.GetSet;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hitasoft on 22/4/18.
 */

public class SosAlert extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "SosAlert";
    ImageView backBtn;
    LinearLayout callPolice, alertEmergency;
    ApiInterface apiInterface;
    double mylat = 0, mylon = 0;
    ArrayList<ProfileData.EmergencyContact> emergencyList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sosalert);

        backBtn = findViewById(R.id.backBtn);
        callPolice = findViewById(R.id.callPolice);
        alertEmergency = findViewById(R.id.alertEmergency);

        mylat = (double) getIntent().getExtras().get("lat");
        mylon = (double) getIntent().getExtras().get("lon");
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        backBtn.setVisibility(View.VISIBLE);

        backBtn.setOnClickListener(this);
        callPolice.setOnClickListener(this);
        alertEmergency.setOnClickListener(this);

        getProfileData();
    }

    private void getProfileData() {
        Call<ProfileData> call3 = apiInterface.profile(GetSet.getToken(), GetSet.getUserId());
        Log.v(TAG, "token=" + GetSet.getToken());
        Log.v(TAG, "userId=" + GetSet.getUserId());
        call3.enqueue(new Callback<ProfileData>() {
            @Override
            public void onResponse(Call<ProfileData> call, Response<ProfileData> response) {
                try {
                    ProfileData data = response.body();
                    Log.v(TAG, "profileData=" + data.user_image);
                    if (data.status.equals("true")) {
                        emergencyList.addAll(data.emergency_contact);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ProfileData> call, Throwable t) {
                call.cancel();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                finish();
                break;
            case R.id.callPolice:
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setData(Uri.parse("tel:" + Constants.emergencycontact));
                startActivity(intent);
                break;
            case R.id.alertEmergency:
                String emergency = "";
                for (int i = 0; i < emergencyList.size(); i++) {
                    if (i == 0)
                        emergency = emergencyList.get(i).phone_no;
                    else
                        emergency = emergency + "," + emergencyList.get(i).phone_no;
                }
                Intent smsIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:" + emergency));
                smsIntent.putExtra("sms_body", "Emergency: MyLocation: https://www.google.com/maps/?q=" + mylat + "," + mylon);
                startActivity(smsIntent);
                break;
        }
    }
}
