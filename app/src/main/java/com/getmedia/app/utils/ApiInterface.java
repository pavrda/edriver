package com.hitasoft.app.utils;

import com.hitasoft.app.model.AvailableRide;
import com.hitasoft.app.model.ConfirmRideData;
import com.hitasoft.app.model.HelpData;
import com.hitasoft.app.model.NotificationData;
import com.hitasoft.app.model.ProfileData;
import com.hitasoft.app.model.RequestRideData;
import com.hitasoft.app.model.RideHistoryData;
import com.hitasoft.app.model.UserData;
import com.hitasoft.app.model.WalletHistoryData;

import java.util.HashMap;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by hitasoft on 24/1/18.
 */

public interface ApiInterface {

    @GET("api/admindatas")
    Call<HashMap<String, String>> adminData();

    @FormUrlEncoded
    @POST("api/signup")
    Call<UserData> signup(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("api/signin")
    Call<UserData> signin(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("api/resetpassword")
    Call<HashMap<String, String>> forgotPassword(@FieldMap Map<String, String> params);

    @GET("api/profile/{user_id}")
    Call<ProfileData> profile(@Header("Authorization") String user_token, @Path("user_id") String user_id);

    @Multipart
    @POST("api/uploadprofileimage")
    Call<Map<String, String>> uploadProfileImage(@Header("Authorization") String user_token, @Part MultipartBody.Part image, @Part("user_id") RequestBody user_id);

    @GET("api/helppages")
    Call<HelpData> getHelp();

    @GET("api/notifications/{user_id}")
    Call<NotificationData> getNotifications(@Header("Authorization") String user_token, @Path("user_id") String user_id);

    @FormUrlEncoded
    @POST("api/changepassword")
    Call<Map<String, String>> changePassword(@Header("Authorization") String user_token, @FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("api/contactus")
    Call<Map<String, String>> contactus(@Header("Authorization") String user_token, @FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("api/updateprofile")
    Call<Map<String, String>> updateProfile(@Header("Authorization") String user_token, @FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("api/grabrides")
    Call<AvailableRide> grabRides(@Header("Authorization") String user_token, @FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("api/confirmride")
    Call<ConfirmRideData> confirmRide(@Header("Authorization") String user_token, @FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("api/requestride")
    Call<RequestRideData> requestRide(@Header("Authorization") String user_token, @FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("api/review")
    Call<Map<String, String>> review(@Header("Authorization") String user_token, @FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("api/addmoney")
    Call<Map<String, String>> addMoney(@Header("Authorization") String user_token, @FieldMap Map<String, String> params);

    @GET("api/wallethistory/{user_id}")
    Call<WalletHistoryData> walletHistory(@Header("Authorization") String user_token, @Path("user_id") String user_id);

    @FormUrlEncoded
    @POST("api/ridehistory")
    Call<RideHistoryData> rideHistory(@Header("Authorization") String user_token, @FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("api/getclienttoken")
    Call<Map<String, String>> getclienttoken(@Header("Authorization") String user_token, @FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("api/cancelride")
    Call<Map<String, String>> cancelRide(@Header("Authorization") String user_token, @FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("api/completeridedetails")
    Call<Map<String, String>> completeridedetails(@Header("Authorization") String user_token, @FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("api/ridedetails")
    Call<Map<String, String>> ridedetails(@Header("Authorization") String user_token, @FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("api/paybycard")
    Call<Map<String, String>> paybycredit(@Header("Authorization") String user_token, @FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("api/paybycash")
    Call<Map<String, String>> paybycash(@Header("Authorization") String user_token, @FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("api/pushsignin")
    Call<Map<String, String>> pushsignin(@Header("Authorization") String user_token, @FieldMap Map<String, String> params);

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "api/pushsignout", hasBody = true)
    Call<Map<String, String>> pushsignout(@Header("Authorization") String user_token, @FieldMap Map<String, String> params);
}
