package com.hitasoft.app.utils;

/**
 * Created by hitasoft on 12/3/18.
 */

public class Constants {

    public final static String CONNECTIVITY_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";

    public final static String BASE_URL = "http://cabsoapp.com/";
    public static final String SOCKETURL = "http://cabsoapp.com:8083";


 /*     public final static String BASE_URL = "http://192.168.1.27:3000/";
      public static final String SOCKETURL = "http://192.168.1.27:8083";*/


    // JSON constant keys
    public static final String TAG_STATUS = "status";
    public static final String TAG_MESSSAGE = "message";
    public static final String TAG_IMAGE = "image";
    public static final String TAG_USER_ID = "user_id";
    public static final String TAG_USER_IMAGE = "user_image";
    public static final String TAG_IMAGE_URL = "imageurl";
    public static final String TAG_FULL_NAME = "full_name";
    public static final String TAG_EMAIL = "email";
    public static final String TAG_PASSWORD = "password";
    public static final String TAG_COUNTRY_CODE = "country_code";
    public static final String TAG_PHONE_NUMBER = "phone_number";
    public static final String TAG_NEW_PASSWORD = "newpassword";
    public static final String TAG_EMERGENCY_CONTACT = "emergency_contact";
    public static String currency_symbol = "$";
    public static String emergencycontact = "";
    public static String LANGUAGE = "English";
    public static int DIALOG_DELAY = 2000;
}
